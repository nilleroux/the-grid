/*! \file Controller.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include <map>

#include "Controller.hpp"


Controller::Controller(Model &model, View &view) : _model(&model), _view(&view)
{}


void Controller::act()
{
    const std::map<Screen, void(Controller::*)()> fcts =
            { { Screen::MENU, &Controller::menu }, { Screen::OPTIONS, &Controller::options },
              { Screen::HIGHSCORES, &Controller::highscores }, { Screen::GAME_SELECT_CAMPAIGN, &Controller::gameSelectCampaign },
              { Screen::GAME_INIT, &Controller::gameInit }, { Screen::GAME, &Controller::game },
              { Screen::LEVEL_END, &Controller::levelEnd }, { Screen::EDITOR, &Controller::editor } };
    if (fcts.find(_model->getTopScreen()) != fcts.end())
        (this->*fcts.find(_model->getTopScreen())->second)(); // call the function associated with the current screen
}


void Controller::nextScreen(Screen screen)
{
    _model->pushScreen(screen);
}

void Controller::prevScreen()
{
    _model->popScreen();
}


void Controller::startGame()
{
    if (_model->getTopScreen() == Screen::GAME_INIT)
        _model->popScreen();
    _model->pushScreen(Screen::GAME);

    _model->getGameData().resetLevelClock();
}

void Controller::nextLevel()
{
    if (!_model->getGameData().getPlayer().hasLost()) // if the player has not lost
        _model->getGameData().nextLevel(); // then we can set up the new level.
    else if (_model->getGameData().getPlayer().getScores().getLives() <= 0) // if the player has no lives remaining
        _model->getGameData().restartGame(); // start a new game.
    else
        _model->getGameData().loseLevel(); // the player has lost.

    // game is relaunched except if a campaign has just been finished
    if (!_model->getGameData().isCampaignFinished())
    {
        prevScreen();
        nextScreen(Screen::GAME);
    }
}

void Controller::exit()
{
    while (_model->getTopScreen() != Screen::NONE)
        _model->popScreen();
}
