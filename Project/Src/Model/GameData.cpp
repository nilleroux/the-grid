/*! \file GameData.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include <fstream>
#include <sstream>
#include <algorithm>

#include "GameData.hpp"
#include "Model.hpp"
#include "GameNormalCell.hpp"
#include "GameBombCell.hpp"
#include "GameBonusCell.hpp"


const std::vector<std::pair<int, int>> GameData::deltas =
        { { -1, 1 }, { 0, 1 }, { 1, 1 }, { -1, 0 }, { 0, 0 }, { 1, 0 }, { -1, -1 }, { 0, -1 }, { 1, -1 } };
const int GameData::max_stored_scores = 10;


GameData::GameData() : _difficulty(Difficulty::MEDIUM), _levelNum(0), _target(0)
{}


std::vector<std::string> GameData::getCampaignNames()
{
    std::vector<std::string> campaign_names;
    int campaign_num = 1;
    std::ostringstream campaign_info_filename;
    campaign_info_filename << "Levels/Campaign" << campaign_num << "/Campaign.info"; // The campaign info file is this.
    std::ifstream campaign_info_file(campaign_info_filename.str().c_str(), std::ios::in | std::ios::binary);
    std::string campaign_name;
    while (campaign_info_file.good()) // While there is a file named as before, a campaign exists.
    {
        campaign_name.clear();
        campaign_info_file >> campaign_name; // We can get its name
        campaign_names.push_back(campaign_name);
        campaign_info_file.close();
        campaign_num++;
        campaign_info_filename.str(std::string());
        campaign_info_filename << "Levels/Campaign" << campaign_num << "/Campaign.info";
        campaign_info_file.open(campaign_info_filename.str().c_str(), std::ios::in | std::ios::binary);
    }
    return campaign_names;
}


int GameData::getLevelNum() const
{
    return _levelNum;
}

int GameData::getTarget() const
{
    return _target;
}

std::chrono::milliseconds GameData::getTimeLimit() const
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(_timeLimit);
}

std::chrono::milliseconds GameData::getElapsedTime() const
{
    // elapsed time = (NOW - start time) + malus - bonus
    return std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::steady_clock::now() - _startTimePoint + _timeLost - _player.getScores().getTimeBonus());
}

const GameGrid &GameData::getGrid() const
{
    return _grid;
}

const Player &GameData::getPlayer() const
{
    return _player;
}

const LevelSolver &GameData::getLevelSolver() const
{
    return _levelSolver;
}

bool GameData::playerHasWon() const
{
    return (_player.getScores().getMoves() >= _target);
}

bool GameData::isCampaignFinished() const
{
    return _campaignFinished;
}

std::string GameData::toString(bool hidden) const
{
    std::string result;
    PlayerCellState player_state;
    int x, y;

    result = " +" + std::string(getGrid().getWidth() * 3, '-') + "+\n";
    for (y = 0; y < getGrid().getHeight(); y++)
    {
        result += " |";
        for (x = 0; x < getGrid().getWidth(); x++)
        {
            player_state = ((getPlayer().getX() == x) && (getPlayer().getY() == y)) ?
                    ((getPlayer().hasLost()) ? PlayerCellState::DEADONCELL : PlayerCellState::ONCELL)
                  : PlayerCellState::ELSEWHERE;
            if (hidden)
                result += " - ";
            else
                result += getGrid()(x, y)->toString(player_state);
        }
        result += "|\n";
    }
    result += " +" + std::string(getGrid().getWidth() * 3, '-') + "+\n";

    return result;
}


void GameData::resetLevelClock()
{
    _startTimePoint = std::chrono::steady_clock::now();
    _timeLost = std::chrono::steady_clock::duration(0);
}

void GameData::loseTime(const std::chrono::seconds &time_to_lose)
{
    _timeLost += std::chrono::duration_cast<std::chrono::steady_clock::duration>(time_to_lose);
}

void GameData::checkTimeLeft()
{
    if (getElapsedTime() >= getTimeLimit())
        _player.lose();
}

void GameData::addTimeLeftBonus()
{
    const std::chrono::milliseconds elapsed_time = getElapsedTime();
    if (_timeLimit > elapsed_time) // if time is not out
        _player.getScores().addTimeLeftBonus(std::chrono::duration_cast<std::chrono::milliseconds>(_timeLimit - elapsed_time));
}


bool GameData::loadGridFromFile(const std::string &filepath)
{
    std::ifstream grid_file(filepath);
    int cell_num, time_limit, w, h;
    char cell_type;
    int cell_value, bonus_value;

    if (!grid_file.good())
        return false;

    grid_file >> _target >> time_limit; // We first get target and time limit on a single line.
    _timeLimit = std::chrono::duration_cast<std::chrono::steady_clock::duration>(std::chrono::milliseconds(time_limit * 1000));
    grid_file >> w >> h; // Then the grid dimension
    _grid.resize(w, h); // Resize the grid to the new size.

    for (cell_num = 0; cell_num < w * h; cell_num++)
    {
        grid_file >> cell_type;
        switch (cell_type)
        {
            case 'N' : // Normal Cell
                grid_file >> cell_value;
                _grid[cell_num] = new GameNormalCell(cell_value);
                break;
            case 'S' : // Score bonus cell
                grid_file >> cell_value >> bonus_value;
                _grid[cell_num] = new GameScoreBonusCell(cell_value, bonus_value);
                break;
            case 'T' : // Time bonus cell
                grid_file >> cell_value >> bonus_value;
                _grid[cell_num] = new GameTimeBonusCell(cell_value, std::chrono::seconds(bonus_value));
                break;
            case 'L' : // Life bonus cell
                grid_file >> cell_value;
                _grid[cell_num] = new GameLifeBonusCell(cell_value);
                break;
            case 'B' : // Bomb cell
                _grid[cell_num] = new GameBombCell();
                break;
            case 'P' : // Player cell
                _grid[cell_num] = new GameNormalCell(0);
                _player.setPos(cell_num / w, cell_num % w);
                break;
            default : // Every other character is incorrect
                return false;
        }
    }

    // Player must always be on a crossed cell
    _grid(_player.getX(), _player.getY())->cross(_player);
    _player.getScores().undoMove();

    _levelSolver = LevelSolver(this); // And we can initialize the levelsolver
    _levelSolver.fillTree();

    resetLevelClock(); // and the clock

    return true; // The grid has been correctly loaded
}


void GameData::generateGrid(int w, int h, int min_value, int max_value, int bombs, int bonuses)
{
    int cell_num = 1, type_num;
    GameCell* player_place;
    _grid.resize(w, h); // Resize the grid to the new size.

    // Prepare a cell for the player's first place.
    player_place = _grid[0] = new GameNormalCell(0);
    // Bombs
    for (type_num = 0; type_num < bombs; type_num++)
    {
        _grid[cell_num] = new GameBombCell();
        cell_num++;
    }
    // Score Bonuses
    for (type_num = 1; type_num <= bonuses * 2 / 5; type_num++)
    {
        _grid[cell_num] = new GameScoreBonusCell(Model::getRandomInt(min_value, max_value), Model::getRandomInt(400, 1000));
        cell_num++;
    }
    // Time bonuses
    for (; type_num <= bonuses * 4 / 5; type_num++)
    {
        _grid[cell_num] = new GameTimeBonusCell(Model::getRandomInt(min_value, max_value),
                std::chrono::milliseconds(Model::getRandomInt(4000, 10000)));
        cell_num++;
    }
    // Life bonuses
    for (; type_num <= bonuses; type_num++)
    {
        _grid[cell_num] = new GameLifeBonusCell(Model::getRandomInt(min_value, max_value));
        cell_num++;
    }
    // Fill remaining cells with normal cells.
    while (cell_num < _grid.getTotalCells())
    {
        _grid[cell_num] = new GameNormalCell(Model::getRandomInt(min_value, max_value));
        cell_num++;
    }

    // Then we can shuffle the grid.
    _grid.shuffle();

    // And to position the player, we must find his cell
    int x = 0, y = 0;
    bool found = false;
    while ((x < _grid.getWidth()) && (!found))
    {
        y = 0;
        while ((y < _grid.getHeight()) && (!found))
            if (_grid(x, y) == player_place)
                found = true;
            else
                y++;
        if (!found)
            x++;
    }
    _player.setPos(x, y);

    // Then we can cross this cell, ignoring the movement in the score.
    _grid(_player.getX(), _player.getY())->cross(_player);
    _player.getScores().undoMove();
}

void GameData::initializeLevel()
{
    _player.getScores().resetLevelScores();

    const int diff_value = static_cast<int>(_difficulty), min_value = diff_value + 1, max_value = std::min(5 + _levelNum / 6, 9),
              grid_width = 16 + _levelNum / 2, grid_height = 16 + _levelNum / 3,
              bombs = 9 + (diff_value + 1) * _levelNum / 2, bonuses = grid_width / 2 - diff_value;
    do
    {
        generateGrid(grid_width, grid_height, min_value, max_value, bombs, bonuses);
        _levelSolver = LevelSolver(this);
        _levelSolver.fillTree();
    }   while (!_levelSolver.isSolvable()); // Generate a grid until it is possible to solve

    resetLevelClock();
}

void GameData::newGame(Difficulty diff)
{
    _difficulty = diff;
    _campaignNum = 0;
    _campaignFinished = false;
    _levelNum = 1;
    _target = (static_cast<int>(_difficulty) + 1) * 10;
    _timeLimit = std::chrono::duration_cast<std::chrono::steady_clock::duration>(std::chrono::seconds(30));
    _player = Player();
    _player.getScores() = ScoresList(3);
    initializeLevel();
}

void GameData::newGame(int campaign_num)
{
    _campaignNum = campaign_num;
    _campaignFinished = false;
    _levelNum = 1;

    int player_lives = 3;
    std::ostringstream campaign_info_filename;
    campaign_info_filename << "Levels/Campaign" << _campaignNum << "/Campaign.info";
    std::ifstream campaign_info_file(campaign_info_filename.str().c_str(), std::ios::in | std::ios::binary);
    std::string line;
    if (campaign_info_file.good())
    {
        std::getline(campaign_info_file, line);
        campaign_info_file >> player_lives;
    }
    _player = Player();
    _player.getScores() = ScoresList(player_lives);

    std::ostringstream filepath;
    filepath << "Levels/Campaign" << _campaignNum << "/Grid" << _levelNum << ".pmap";
    if (!loadGridFromFile(filepath.str()))
        _campaignFinished = true;
}

void GameData::restartGame()
{
    if (_campaignNum > 0) // If there is a campaign running
        newGame(_campaignNum);
    else
        newGame(_difficulty); // Else, launch "random" mode
}

void GameData::endGame(const std::string &player_name)
{
    if (!player_name.empty()) // If player name is filled
    {
        std::vector<std::pair<int, std::string>> scores_list = loadScores(getScoreFilename()); // Save it to the score file name
        if ((static_cast<int>(scores_list.size()) < max_stored_scores)
         || (_player.getScores().getTotalScore() > scores_list.back().first))
        {
            std::pair<int, std::string> score(_player.getScores().getTotalScore(), player_name);
            if (static_cast<int>(scores_list.size()) < max_stored_scores)
                scores_list.push_back(score);
            else
                scores_list.back() = score;
            std::sort(scores_list.begin(), scores_list.end(), std::greater<std::pair<int, std::string>>()); 
            saveScores(scores_list); // And sort the scores in descending order
        }
    }
}

void GameData::loseLevel()
{
    _player.getScores().loseLife();
    _player.resurrect();
    if (_campaignNum > 0) // If there is a campaign running
    {
        std::ostringstream filepath;
        filepath << "Levels/Campaign" << _campaignNum << "/Grid" << _levelNum << ".pmap";
        if (!loadGridFromFile(filepath.str())) // Reset the level
            _campaignFinished = true;
        _player.getScores().resetLevelScores();
        resetLevelClock();
    }
    else // If "random" mode
        initializeLevel(); // initialize the new level
}

void GameData::nextLevel()
{
    _levelNum++;
    if (_campaignNum > 0) // If there is a campaign running
    {
        std::ostringstream filepath;
        filepath << "Levels/Campaign" << _campaignNum << "/Grid" << _levelNum << ".pmap";
        if (!loadGridFromFile(filepath.str())) // Load the next level
        {   // And if there is no file, then the campaign is finished.
            _campaignFinished = true;
            _player.getScores().addCampaignBonus(_levelNum * 1000 + _player.getScores().getLives() * 800);
        }
        _player.getScores().resetLevelScores();
        resetLevelClock();
    }
    else // If "random" mode
    {
        _target += (_levelNum > 5) ? 5 : 10; // Increase difficulty
        _timeLimit += std::chrono::duration_cast<std::chrono::steady_clock::duration>(std::chrono::seconds(4));
        initializeLevel();
    }
}


std::vector<std::pair<int, std::string>> GameData::loadScores() const
{
    if (_campaignNum > 0)
        return loadScores(_campaignNum);
    return loadScores(_difficulty);
}

std::vector<std::pair<int, std::string>> GameData::loadScores(Difficulty diff)
{
    const std::vector<std::string> score_filename_patterns = { "Easy.score", "Medium.score", "Hard.score" };
    return loadScores("Scores/" + score_filename_patterns[static_cast<int>(diff)]);
}

std::vector<std::pair<int, std::string>> GameData::loadScores(int campaign_num)
{
    std::ostringstream filepath;
    filepath << "Scores/Campaign" << campaign_num << ".score";
    return loadScores(filepath.str());
}

std::string GameData::getScoreFilename() const
{
    if (_campaignNum > 0)
    {
        std::ostringstream filepath;
        filepath << "Scores/Campaign" << _campaignNum << ".score";
        return filepath.str();
    }
    const std::vector<std::string> score_filename_patterns = { "Easy.score", "Medium.score", "Hard.score" };
    return "Scores/" + score_filename_patterns[static_cast<int>(_difficulty)];
}

std::vector<std::pair<int, std::string>> GameData::loadScores(const std::string &filename)
{
    std::ifstream score_file(filename.c_str(), std::ios::in | std::ios::binary);
    std::vector<std::pair<int, std::string>> scores_list;
    int score;
    std::string player_name;

    score_file >> score;
    while ((score_file.good()) && (!score_file.eof()))
    {
        score_file.get();
        std::getline(score_file, player_name);
        scores_list.push_back({ score, player_name });
        score_file >> score;
    }
    return scores_list;
}

void GameData::saveScores(const std::vector<std::pair<int, std::string>> &scores_list) const
{
    std::ofstream score_file(getScoreFilename().c_str(), std::ios::out | std::ios::binary);
    if (score_file.good())
        for (const auto &score : scores_list)
            score_file << score.first << ' ' << score.second << std::endl;
}


int GameData::movePlayer(int dx, int dy, bool with_tree)
{
    int movement = 0;
    if ((!dx) && (!dy)) // If there is no move requested
        return 0;
    if (_grid.isOnGrid(_player.getX() + dx, _player.getY() + dy)) // If the next cell is in the grid
    {
        GameNormalCell* game_cell = dynamic_cast<GameNormalCell*>(_grid(_player.getX() + dx, _player.getY() + dy));
        if ((game_cell) && (!game_cell->getCrossed())) // If next cell is a normal cell
            movement = game_cell->getValue();
        else
            return 0;
    }
    else
        return 0;

    int moved = 0;
    while ((moved < movement) && (!_player.hasLost())) // While we can move
    {
        if (_grid.isOnGrid(_player.getX() + dx, _player.getY() + dy)) // if next cell is on grid
        {
            _player.move(dx, dy);
            _grid(_player.getX(), _player.getY())->cross(_player);
            moved++;
        }
        else
            _player.lose();
    }

    if ((moved > 0) && (with_tree))
        _levelSolver.goToBranch(dx, dy);
    return moved;
}

void GameData::unmovePlayer(int dx, int dy, int move)
{
    _player.resurrect(); // The player will necessarily be alive

    int unmoved;
    for (unmoved = 0; unmoved < move; unmoved++)
    {
        _grid(_player.getX(), _player.getY())->uncross(_player); // uncross each cell
        _player.move(-dx, -dy); // go to the opposite direction
    }
}

int GameData::movePlayer(int dx, int dy)
{
    return movePlayer(dx, dy, true);
}

bool GameData::unmovePlayer()
{
    const Movement move = _levelSolver.comeBackFromBranch();
    if (move.movement)
    {
        unmovePlayer(move.dx, move.dy, move.movement);
        return true;
    }
    else
        return false;
}


void GameData::autodestruct()
{
    _player.lose();
}
