var searchData=
[
  ['uncross',['uncross',['../class_game_cell.html#afff2ae64101722c3a8e2ebc75a67fea9',1,'GameCell']]],
  ['undobonus',['undoBonus',['../class_game_bonus_cell.html#a640bda72afee66f6607290a9d50250b1',1,'GameBonusCell::undoBonus()'],['../class_game_time_bonus_cell.html#a95f55f1c57203b6fbafb652158ada049',1,'GameTimeBonusCell::undoBonus()'],['../class_game_score_bonus_cell.html#a65f47c690aaf178e05e3a3f6f95f3bd7',1,'GameScoreBonusCell::undoBonus()'],['../class_game_life_bonus_cell.html#ae52df55aac93ac08cab459da7c498c8e',1,'GameLifeBonusCell::undoBonus()']]],
  ['undocellscore',['undoCellScore',['../class_scores_list.html#a314e8c447f056798fde7d22d0f50d86f',1,'ScoresList']]],
  ['undocrossing',['undoCrossing',['../class_game_bomb_cell.html#a317f4e9002208c38e31c70f13cbaff2e',1,'GameBombCell::undoCrossing()'],['../class_game_bonus_cell.html#a31a2292f593c5d29f011d82affd063fb',1,'GameBonusCell::undoCrossing()'],['../class_game_cell.html#acf98723737491f91ed95afc1f3510970',1,'GameCell::undoCrossing()'],['../class_game_normal_cell.html#a7820fa9d918ba9620c68285ba08192ef',1,'GameNormalCell::undoCrossing()']]],
  ['undolastmove',['undoLastMove',['../class_console_controller.html#a402be03e4472945de96fd3f8f26ca3f7',1,'ConsoleController']]],
  ['undolifebonus',['undoLifeBonus',['../class_scores_list.html#adf338830509e2a695ef788d47ec7477a',1,'ScoresList']]],
  ['undomove',['undoMove',['../class_scores_list.html#a6f899853b6e67996f797e98f0a3b5201',1,'ScoresList']]],
  ['undoscorebonus',['undoScoreBonus',['../class_scores_list.html#afd076a25ef70cec932e3cfa05ee46dc9',1,'ScoresList']]],
  ['undotimebonus',['undoTimeBonus',['../class_scores_list.html#a0c4dff18b96cc6f67b4de9ba1d8c9f77',1,'ScoresList']]],
  ['unmoveplayer',['unmovePlayer',['../class_game_data.html#a485f0deef1c06c35fef1450187a18a23',1,'GameData']]]
];
