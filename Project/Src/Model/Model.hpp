/*! \file Model.hpp
 * \brief The model.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_MODEL_HPP
#define PPD_MODEL_HPP


#include <string>
#include <unordered_map>
#include <stack>
#include <random>

#include "GameData.hpp"


/// Available screens
enum class Screen
{
    NONE,                   ///< No Screen
    MENU,                   ///< Menu screen
    OPTIONS,                ///< Options menu screen
    HIGHSCORES,             ///< Highscores display screen
    GAME_SELECT_CAMPAIGN,   ///< Campaign selection screen
    GAME_INIT,              ///< Game initialisation screen
    GAME,                   ///< Game screen
    LEVEL_END,              ///< End of level screen
    EDITOR                  ///< Level editor screen
};


/*! \class Model
* \brief The model of the Game
*/
class Model
{
        static std::mt19937 _randomEngine; ///< The random engine

        std::unordered_map<std::string, std::string> _options, ///< User's options
                                                     _labels; ///< Labels
        std::vector<std::pair<std::string, std::string>> _languages; ///< Languages available (eg : "english", "English")

        std::stack<Screen> _screens; ///< Stack of screens loaded

        GameData _gameData; ///< Data about the game.

    public :
        /*! 
         *  \brief Model default constructor
         */
        Model();
        /*! 
         *  \brief Model destructor
         */
        ~Model();

    private :
        /*! 
         *  \brief Loads default options from file
         */
        void loadOptions();
        /*! 
         *  \brief Loads the list of available languages from file
         */
        void loadLanguagesList();
        /*! 
         *  \brief Loads labels corresponding to the current language option.
         */
        void loadLabels();

        /*! 
         *  \brief Save current options to a file.
         */
        void saveOptions() const;

    public :
        /*! 
         *  \brief Allows to retrieve an option
         *  \param[in] name : The option key
         *  \return The option value
         */
        std::string getOption(const std::string &name) const;
        /*! 
         *  \brief Current difficulty level getter
         *  \return The difficulty level
         */
        Difficulty getDifficulty() const;

        /*! 
         *  \brief Allows to retreive a label
         *  \param[in] key : The label key
         *  \return The label
         */
        std::string getLabel(const std::string &key) const;
         /*! 
         *  \brief Allows to retreive a vectore of every language available.
         *  \return A vector containing languages key and name.
         */
        const std::vector<std::pair<std::string, std::string>> &getLanguages() const;

         /*! 
         *  \brief A simple GameData getter.
         */
        const GameData &getGameData() const;
        /*! 
         *  \brief A simple GameData getter.
         */
        GameData &getGameData();

        /*! 
         *  \brief Allows to change a given option.
         *  \param[in] key : The option key
         *  \param[in] new_value : The new option value
         */
        void setSpecificOption(const std::string &key, const std::string &new_value);
        /*! 
         *  \brief Allows to change the current language.
         *  \param[in] new_lang The key of the new language desired
         */
        void changeLanguage(const std::string &new_lang);
        /*! 
         *  \brief Allows to change the difficulty of the game.
         *  \param[in] new_diff The new difficulty desired
         */
        void changeDifficulty(Difficulty new_diff);

        /*! 
         *  \brief Random int betwin min and max. Generated using the random engine.
         *  \param[in] min : The min value
         *  \param[in] max : The max value
         *  \return The random value
         */
        static int getRandomInt(int min, int max);
        /*! 
         *  \brief Random float betwin min and max. Generated using the random engine.
         *  \param[in] min : The min value
         *  \param[in] max : The max value
         *  \return The random value
         */
        static float getRandomFloat(float min, float max);
        /*! 
         *  \brief Returns the length of an UTF-8 encoded string.
         *  \param[in] str : the string
         *  \return The length of the string
         */
        static size_t getStringLength(const std::string &str);
        /*! 
         *  \brief Returns the length of an integer.
         *  \param[in] nb : the integer
         *  \return The length of the integer
         */
        static size_t getIntegerLength(int nb);

        /*! 
         *  \brief Pushes a given screen to the top of the screens stack.
         *  \param[in] screen : The screen to push
         */
        void pushScreen(Screen screen);
        /*! 
         *  \brief Top screen getter.
         *  \return The screen on the top of the screens stack.
         */
        Screen getTopScreen() const;
        /*! 
         *  \brief Pops the screen on the top of the screens stack.
         */
        void popScreen();

};


#endif
