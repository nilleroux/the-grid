/*! \file Model.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include <cmath>

#include <fstream>
#include <sstream>
#include <map>
#include <chrono>

#include "Model.hpp"


std::mt19937 Model::_randomEngine(time(NULL));


Model::Model()
{
    srand(time(NULL));

    loadOptions();
    loadLanguagesList();
    loadLabels();
}

Model::~Model()
{
    saveOptions();
}


void Model::loadOptions()
{
    /*
    File format:
        key1 value1
        key2 value2
        key3 value3
    */

    // Loading the file.
    std::ifstream options_file("Options/Options.cfg", std::ios::in | std::ios::binary);
    std::string option_name, option_value;

    while ((options_file.good()) && (!options_file.eof()))
    {
        options_file >> option_name; // getting the option name
        options_file.get();
        if (!option_name.empty())
        {
            std::getline(options_file, option_value); // and the option value
            _options.insert({ option_name, option_value }); // inserting it to the options
        }
        option_name.clear();
    }
}

void Model::loadLanguagesList()
{
    /*
    File format:
        Language_Key1
        Language_Value1
        Language_Key2
        Language_Value2

    Please note that Language_Value is the rootname of the associated .lng file.
    */

    // Loading languages list file
    std::ifstream languages_file("Languages/Index.cfg", std::ios::in | std::ios::binary);
    std::string language_name, language_filename;

    while ((languages_file.good()) && (!languages_file.eof()))
    {
        std::getline(languages_file, language_name); // getting langage name
        if (!language_name.empty())
        {
            std::getline(languages_file, language_filename); // getting the value (~filename)
            _languages.push_back({ language_name, language_filename }); // pushing it to the available languages
        }
    }
}

void Model::loadLabels()
{
    /*
    File format:
        option_1_key number_of_line_of_option_1_value =
        option_1_value

        option_2_key number_of_line_of_option_2_value =
        option_2_value
    */
    std::string language_filename;
    for (const auto &language : _languages) // get the root of the filename
        if (language.first == getOption("language"))
            language_filename = language.second;

    // Loading the correct .lng file
    std::ifstream labels_file("Languages/" + language_filename + ".lng", std::ios::in | std::ios::binary);
    std::string header, line, key, value;
    int lines_nb, line_num;

    _labels.clear();
    while ((labels_file.good()) && (!labels_file.eof()))
    {
        std::getline(labels_file, header);
        if (!header.empty())
        {
            std::istringstream input_buf(header);
            lines_nb = 0;
            input_buf >> key >> lines_nb; // getting option's key and value's number of line
            value.clear();
            for (line_num = 0; line_num < lines_nb; line_num++) // Then reading the value
            {
                if (line_num > 0)
                    value += '\n'; // Adding line break if option's value is taller than 1 line.
                std::getline(labels_file, line);
                value += line;
            }
            _labels.insert({ key, value });
        }
    }
}


void Model::saveOptions() const
{
    // Loading file with writing permissions (file will be erased)
    std::ofstream options_file("Options/Options.cfg", std::ios::out | std::ios::binary);
    for (const auto &option : _options) // Saving each option
        options_file << option.first << ' ' << option.second << std::endl;
}


std::string Model::getOption(const std::string &name) const
{
    if (_options.find(name) != _options.end())
        return _options.at(name);
    return std::string();
}

Difficulty Model::getDifficulty() const
{
    const std::map<std::string, Difficulty> difficulties =
            { { "easy", Difficulty::EASY }, { "medium", Difficulty::MEDIUM }, { "hard", Difficulty::HARD } };
    if (difficulties.find(getOption("difficulty")) != difficulties.end())
        return difficulties.at(_options.at("difficulty"));
    return Difficulty::LAST;
}


std::string Model::getLabel(const std::string &key) const
{
    if (_labels.find(key) != _labels.end())
        return _labels.at(key);
    return "“" + key + "”";
}

const std::vector<std::pair<std::string, std::string>> &Model::getLanguages() const
{
    return _languages;
}


const GameData &Model::getGameData() const
{
    return _gameData;
}

GameData &Model::getGameData()
{
    return _gameData;
}


void Model::setSpecificOption(const std::string &key, const std::string &new_value)
{
    _options[key] = new_value;
}

void Model::changeLanguage(const std::string &new_lang)
{
    _options["language"] = new_lang;
    loadLabels(); // Do not forget to reload labels
}

void Model::changeDifficulty(Difficulty new_diff)
{
    const std::map<Difficulty, std::string> difficulties =
            { { Difficulty::EASY, "easy" }, { Difficulty::MEDIUM, "medium" }, { Difficulty::HARD, "hard" } };
    _options["difficulty"] = difficulties.at(new_diff);
}


int Model::getRandomInt(int min, int max)
{
    std::uniform_int_distribution<int> distrib(min, max);
    return distrib(_randomEngine);
}

float Model::getRandomFloat(float min, float max)
{
    std::uniform_real_distribution<float> distrib(min, max);
    return distrib(_randomEngine);
}

size_t Model::getStringLength(const std::string &str)
{
    size_t len = 0;
    int crt_buf;
    for (char crt : str)
    {
        crt_buf = (crt + 256) % 256; // Be sure to have an unsigned value
        if ((crt_buf < 128) || (192 <= crt_buf)) // An UTF-8 character begins with a byte in this range
            len++;
    }
    return len;
}

size_t Model::getIntegerLength(int nb)
{
    if (!nb)
        return 1;
    return (nb < 0) ? (static_cast<size_t>(floor(log10(-nb))) + 2) : (static_cast<size_t>(floor(log10(nb))) + 1);
}


void Model::pushScreen(Screen screen)
{
    _screens.push(screen);
}

Screen Model::getTopScreen() const
{
    if (!_screens.empty())
        return _screens.top();
    return Screen::NONE;
}

void Model::popScreen()
{
    if (!_screens.empty())
        _screens.pop();
}
