#!/bin/bash


shopt -s dotglob extglob globstar

MAKEFILE=Makefile


cp -f Makefile.beg "$MAKEFILE"


if [ "$(echo "$1" | cut -c 1-5)" = Linux ]; then
    DEFINES=' -DLINUX_CONSOLE_COLOR'
fi


for src_file in ../Src/**/*.cpp; do
    echo -n '$(OBJDIR)/' >> "$MAKEFILE"
    g++ -MM -std=c++11 $src_file >> "$MAKEFILE"
    echo $'\t$(CXX) -c $< -o $@ $(CXXFLAGS)'"$DEFINES" >> "$MAKEFILE"
done
for src_file in ../Tests/**/*.cpp; do
    echo -n '$(OBJDIR)/' >> "$MAKEFILE"
    g++ -MM -std=c++11 $src_file >> "$MAKEFILE"
    echo $'\t$(CXX) -c $< -o $@ $(CXXFLAGS)'"$DEFINES" >> "$MAKEFILE"
done


if [ "$(echo "$1" | cut -c 1-3)" = Win ]; then
    make -f "$MAKEFILE" TARGET="$1" RES="../Out/$1/Obj/icon.res" $2
elif [ "$1" ]; then
    make -f "$MAKEFILE" TARGET="$1" $2
fi
