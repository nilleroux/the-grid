/*! \file GameData.hpp
 * \brief Deals with in-game data (player, grid…).
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_GAMEDATA_HPP
#define PPD_GAMEDATA_HPP


#include <chrono>

#include "GameGrid.hpp"
#include "Player.hpp"
#include "LevelSolver.hpp"


/// Available difficulties levels
enum class Difficulty
{
    EASY,   ///< Easy difficulty
    MEDIUM, ///< Medium difficulty
    HARD,   ///< Hard difficulty
    LAST    ///< The last difficulty level
};


/*! \class GameData
* \brief GameData is the class used to describe data during the game.
*/
class GameData
{
    public :
        static const std::vector<std::pair<int, int>> deltas; ///< Vector of every possible move over the grid in the numpad order.
        static const int max_stored_scores; ///< Number of high scores stored for each game mode/campaign

    private :
        Difficulty _difficulty; ///< Difficulty level
        int _campaignNum, ///< Campaign number
            _levelNum, ///< Level number
            _target; ///< Moves target
        std::chrono::steady_clock::duration _timeLimit, ///< Time limit
                                            _timeLost; ///< Time lost
        std::chrono::steady_clock::time_point _startTimePoint; ///< Level start time
        GameGrid _grid; ///< Level Grid
        Player _player; ///< The player
        LevelSolver _levelSolver; ///< LevelSolver
        bool _campaignFinished; ///< End of campaign flag

    public :
        /*!
         *  \brief GameData default constructor
         */
        GameData();

        /*!
         *  \brief Returns all available campaigns
         *  \return A vector containing each campaign available
         */
        static std::vector<std::string> getCampaignNames();

        /*!
         *  \brief Level number getter
         *  \return The current level number
         */
        int getLevelNum() const;
        /*!
         *  \brief Moves target of the level getter
         *  \return The moves target of the level
         */
        int getTarget() const;
        /*!
         *  \brief Time limit getter
         *  \return The time limit
         */
        std::chrono::milliseconds getTimeLimit() const;
        /*!
         *  \brief Elapsed time getter
         *  \return The elapsed time since the start of the level, including bonuses and malus
         */
        std::chrono::milliseconds getElapsedTime() const;
        /*!
         *  \brief Grid getter
         *  \return A const reference to the game grid.
         */
        const GameGrid &getGrid() const;
        /*!
         *  \brief Player getter
         *  \return A const reference to the player.
         */
        const Player &getPlayer() const;
        /*!
         *  \brief Level Solver getter
         *  \return A const reference to the level solver.
         */
        const LevelSolver &getLevelSolver() const;
        /*!
         *  \brief Checks whether the player has won
         *  \return true if the player has won, false otherwise.
         */
        bool playerHasWon() const;
        /*!
         *  \brief Checks whether the player has finished the campaign
         *  \return true if the campaign is finished, false otherwise
         */
        bool isCampaignFinished() const;
        /*!
         *  \brief toString implementation for the GameData. It includes game grid and scores.
         *  \param[in] hidden : hide grid cells with "X" char. Useful for introduction screen.
         *  \return the string value of this GameData
         */
        std::string toString(bool hidden = false) const;

        /*!
         *  \brief Resets the clock (startTimePoint and timeLost).
         */
        void resetLevelClock();
        /*!
         *  \brief Adds time to _timeLost.
         *  \param[in] time_to_lose : Time to add to _timeLost
         */
        void loseTime(const std::chrono::seconds &time_to_lose);
        /*!
         *  \brief Checks whether time is out and kills the player if so.
         */
        void checkTimeLeft();
        /*!
         *  \brief Adds timeBonus to the player.
         */
        void addTimeLeftBonus();

    private :
        /*!
         *  \brief Loads a grid from a file
         *  \param[in] filepath : the file path
         *  \return true : success, false : failure
         */
        bool loadGridFromFile(const std::string &filepath);

        /*!
         *  \brief Generates a new grid
         *  \param[in] w : Width of the new grid
         *  \param[in] h : Height of the new grid
         *  \param[in] min_value : Minimum value of numbered cells
         *  \param[in] max_value : Maximum value of numbered cells
         *  \param[in] bombs : Amount of bombs in the grid
         *  \param[in] bonuses : Amount of bonuses in the grid
         *  The grid is not necessarily possible to solve.
         */
        void generateGrid(int w, int h, int min_value, int max_value, int bombs, int bonuses);
        /*!
         *  \brief Initializes a new random solvable grid.
         */
        void initializeLevel();
    public :
         /*!
         *  \brief Starts a new game (from the beginning) of given difficulty level
         *  \param[in] diff : The difficulty level of the new game
         */
        void newGame(Difficulty diff = Difficulty::MEDIUM);
        /*!
         *  \brief Starts a new game in campaign mode with the given campaign number
         *  \param[in] campaign_num : The number of the campaign to load
         */
        void newGame(int campaign_num);
        /*!
         *  \brief Starts a new game with the same properties.
         */
        void restartGame();
        /*!
         *  \brief Ends a game updating high scores
         *  \param[in] player_name : the name of the player
         */
        void endGame(const std::string &player_name);
        /*!
         *  \brief Updates the player and initializes a new level.
         */
        void loseLevel();
        /*!
         *  \brief Sets up the next level.
         */
        void nextLevel();

        /*!
         *  \brief Loads scores from file (current mode)
         *  \return a vector containing the score and the name of each player
         */
        std::vector<std::pair<int, std::string>> loadScores() const;
         /*!
         *  \brief Loads scores from file (Random mode)
         *  \param[in] diff : the difficulty level of Random mode
         *  \return a vector containing the score and the name of each player
         */
        static std::vector<std::pair<int, std::string>> loadScores(Difficulty diff);
        /*!
         *  \brief Loads scores from file (Campaign mode)
         *  \param[in] campaign_num : the campaign_number
         *  \return a vector containing the score and the name of each player
         */
        static std::vector<std::pair<int, std::string>> loadScores(int campaign_num);
    private :
        /*!
         *  \brief Returns the highscore filepath accordingly to the current mode
         *  \return The filepath
         */
        std::string getScoreFilename() const;
        /*!
         *  \brief Loads game type scores
         *  \return return a vector of score-and-name pairs
         */
        static std::vector<std::pair<int, std::string>> loadScores(const std::string &filename);
        /*!
         *  \brief Save new scores to a file accordingly to the current mode.
         *  \param[in] scores_list : a vector of scores and player names.
         */
        void saveScores(const std::vector<std::pair<int, std::string>> &scores_list) const;

        /*!
         *  \brief Moves the player on the grid.
         *  \param[in] dx : The X direction (-1, 0 or 1)
         *  \param[in] dy : The Y direction (-1, 0 or 1)
         *  \param[in] with_tree : the LevelSolver must be filled at the same time
         *  \return The number of cells crossed during the movement
         */
        int movePlayer(int dx, int dy, bool with_tree);
         /*!
         *  \brief Undoes the last move of the player.
         *  \param[in] dx : The X direction of the move to undo (not the unmove) (-1, 0 or 1)
         *  \param[in] dy : The Y direction  of the move to undo (not the unmove) (-1, 0 or 1)
         *  \param[in] move : The number of cells to uncross
         */
        void unmovePlayer(int dx, int dy, int move);
    public :
         /*!
         *  \brief Moves the player on the grid.
         *  \param[in] dx : The X direction of the move (-1, 0 or 1)
         *  \param[in] dy : The Y direction of the move (-1, 0 or 1)
         *  \return The number of cells uncrossed during the movement
         */
        int movePlayer(int dx, int dy);
        /*!
         *  \brief Undoes player's last movement
         *  \return true : succress, false : failure.
         */
        bool unmovePlayer();
        /*!
         *  \brief Kills the player
         */
        void autodestruct();

    friend class LevelSolver;
};


#endif
