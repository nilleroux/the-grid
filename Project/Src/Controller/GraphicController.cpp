/*! \file GraphicController.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include <sstream>
#include <vector>
#include <map>

#include "GraphicController.hpp"


GraphicController::GraphicController(Model &model, GraphicView &view) : Controller(model, view), _graphicView(&view)
{}


void GraphicController::menu()
{
    _graphicView->prepareMenu("game_title");

    const std::vector<Screen> possible_screens = // Screen that may be chosen
            { Screen::GAME_INIT, Screen::GAME_SELECT_CAMPAIGN, Screen::HIGHSCORES, Screen::EDITOR, Screen::OPTIONS };
    int action;
    do
    {
        _view->draw();
        action = _graphicView->getAction();

        if (action == 1) // before playing, we must set the game up
            _model->getGameData().newGame(_model->getDifficulty()); // accordingly to selected options

        if ((1 <= action) && (action <= static_cast<int>(possible_screens.size()))) // If the action is not exit
            nextScreen(possible_screens[action - 1]);   // Push the appropriate screen
        else if (action) // otherwise, action is exit
            exit();
    }   while (!action); // Wait for an action
}

void GraphicController::options()
{
    _graphicView->prepareMenu("options_title");

    int action;
    do
    {
        _view->draw();
        action = _graphicView->getAction();
        if (action < 0) // If action is quit
            exit();
        else if ((action == 1) || (action == 3)) // If action is "Apply" or "Validate"
        {
            // Change the difficulty
            _model->changeDifficulty(static_cast<Difficulty>(_graphicView->getSelectedChoice(1)));
            // Change the language
            const int chosen_language_num = _graphicView->getSelectedChoice(2);
            if ((0 <= chosen_language_num) && (chosen_language_num < static_cast<int>(_model->getLanguages().size())))
            {
                const std::string chosen_language = _model->getLanguages()[chosen_language_num].first;
                if (_model->getOption("language") != chosen_language)
                    _model->changeLanguage(chosen_language);
            }
             // Change the theme
            const std::vector<std::string> themes = _graphicView->getAvailableThemes();
            const int chosen_theme_num = _graphicView->getSelectedChoice(3);
            if ((0 <= chosen_theme_num) && (chosen_theme_num < static_cast<int>(themes.size()))
             && (_model->getOption("theme") != themes[chosen_theme_num]))
            {
                _model->setSpecificOption("theme", themes[chosen_theme_num]);
                _graphicView->loadTheme(themes[chosen_theme_num]);
            }
            // Change window mode/resolution
            const int window_mode = (_model->getOption("fullscreen") == "y") ? 0 : 1,
                      chosen_window_mode = _graphicView->getSelectedChoice(4);
            if ((chosen_window_mode == 1) && (window_mode == 0))
            {
                _model->setSpecificOption("fullscreen", "n");
                _graphicView->createWindow();
            }
            else if ((chosen_window_mode == 0) && ((window_mode == 1)
                   || (_graphicView->getVideoModes()[_graphicView->getSelectedChoice(5)] != _model->getOption("resolution"))))
            {
                std::istringstream mode_buf(_graphicView->getVideoModes()[_graphicView->getSelectedChoice(5)]);
                int w, h;
                char crt;
                mode_buf >> w >> crt >> crt >> h;
                _model->setSpecificOption("fullscreen", "y");
                _graphicView->createWindow(true, sf::VideoMode(w, h));
            }
            _model->setSpecificOption("resolution", _graphicView->getVideoModes()[_graphicView->getSelectedChoice(5)]);
            // Toggle music
            const int music_enabled = (_model->getOption("music_enabled") != "n") ? 0 : 1,
                      chosen_music_toggle = _graphicView->getSelectedChoice(6);
            if (music_enabled != chosen_music_toggle)
            {
                _model->setSpecificOption("music_enabled", (chosen_music_toggle == 0) ? "y" : "n");
                _graphicView->checkMusicState();
            }
            if (action == 3) // “Validate” leads to previous screen
                prevScreen();
        }
        else if (action == 2)
            _graphicView->prepareMenu("options_title"); // Reset
        else if (action)
            prevScreen();
    }   while (!action); // Wait for an action
}


void GraphicController::highscores()
{
    const std::vector<Difficulty> difficulties = { Difficulty::EASY, Difficulty::MEDIUM, Difficulty::HARD };

    _graphicView->prepareMenu("highscores_title");

    int action;
    do
    {
        _view->draw();
        action = _graphicView->getAction();
        if (action < 0)
            exit();
        else if (action)
        {
            if (action <= (static_cast<int>(difficulties.size()) + 1)) // If there is a highscore table to draw
                highscoresTable(action - 1); // Draw it
            else // Cancel
                prevScreen();
        }
    }   while (!action); // Wait for an action
}

void GraphicController::highscoresTable(int choice)
{
    const std::vector<Difficulty> difficulties = { Difficulty::EASY, Difficulty::MEDIUM, Difficulty::HARD };
    std::vector<std::pair<int, std::string>> scores_list;
    int campaign_num = 1;

    // if a difficulty was selected
    if ((0 <= choice) && (choice < static_cast<int>(difficulties.size())))
        scores_list = GameData::loadScores(difficulties[choice]);
    // otherwise, we can show campaign high scores
    else if (choice == static_cast<int>(difficulties.size()))
    {
        _graphicView->prepareCampaignSelector();
        scores_list = GameData::loadScores(campaign_num);
    }

    _graphicView->prepareScores("highscores_title", scores_list);

    int action;
    do
    {
        _view->draw();
        action = _graphicView->getAction();
        // If we are viewing campain score and we have changed the campaign selector
        if (choice == static_cast<int>(difficulties.size()) && campaign_num != (_graphicView->getSelectedChoice(1) + 1))
        {
            campaign_num = _graphicView->getSelectedChoice(1) + 1;
            scores_list = GameData::loadScores(campaign_num);
            _graphicView->prepareScores("highscores_title", scores_list);
        }
        if (action < 0)
            exit();
    }   while (!action); // Wait again for an action
}


void GraphicController::gameInit()
{
    _graphicView->prepareMenu("start_game");
    _graphicView->setPlayerDirection(4);

    int action;
    do
    {
        _view->draw();
        action = _graphicView->getAction();
        if (action < 0) // Exit action
            exit();
        else if (action == 1) // Start game action
            startGame();
        else if (action)
            prevScreen();
    }   while (!action); // Wait for an action
}

void GraphicController::gameSelectCampaign()
{
    _graphicView->prepareMenu("campaign_title");

    int action;
    do
    {
        _view->draw();
        action = _graphicView->getAction();
        if (action < 0)
            exit();
        else if (action)
        {
            prevScreen();
            if (action == 1) // Start game
            {
                nextScreen(Screen::GAME_INIT); // Push the new screen
                _model->getGameData().newGame(_graphicView->getSelectedChoice(1) + 1); // And start the game
                if (_model->getGameData().isCampaignFinished()) // If there is no level in the campaign, we should pop the top screen
                    prevScreen();
            }
        }
    }   while (!action); // Wait for an action
}

void GraphicController::game()
{
    _graphicView->prepareMenu("playing_title");

    const std::vector<std::pair<int, int>> &deltas = GameData::deltas;
    int action;
    do
    {
        _view->draw();
        _model->getGameData().checkTimeLeft();
        if (_model->getGameData().getPlayer().hasLost()) // If player has lost
            action = 2; // Simulating autodestruction
        else
            action = _graphicView->getAction();
        if (action < 0)
            exit();
        else if (action == 1) // If action is “cancel last move”
        {
            const Movement last_move = _model->getGameData().getLevelSolver().getLastMovement();
            if (last_move.movement)
            {
                const GameGrid old_grid = _model->getGameData().getGrid();
                int delta_num = 0; // find the index of the last movement
                while ((GameData::deltas[delta_num].first != last_move.dx) || (GameData::deltas[delta_num].second != last_move.dy))
                    delta_num++;
                _graphicView->setPlayerDirection(8 - delta_num); // change the player direction
                _model->getGameData().unmovePlayer(); // undo last move
                _model->getGameData().loseTime(std::chrono::seconds(4)); // Do not forget to lose some time
                _graphicView->animatePlayerUnmove(old_grid, last_move.movement); // the view must animate the player unmovement
            }
        }
        else if (action == 2) // If action is “autodestruct”
        {
            _model->getGameData().autodestruct();
            _graphicView->animatePlayerExplosion();
            prevScreen();
            nextScreen(Screen::LEVEL_END);
        }
        else if (action == 50) // If action is “abandon”, save player's score and go to the main menu
        {
            const std::vector<std::pair<int, std::string>> highscores_list = _model->getGameData().loadScores();
            if ((_model->getGameData().getPlayer().getScores().getTotalScore() > 0)
             && ((static_cast<int>(highscores_list.size()) < GameData::max_stored_scores)
              || (_model->getGameData().getPlayer().getScores().getTotalScore() > highscores_list.back().first)))
                saveScoreDialog();
            prevScreen();
        }
        else if ((10 <= action) && (action < 10 + static_cast<int>(deltas.size()))) // If the action is a move
        {
            const GameGrid old_grid = _model->getGameData().getGrid();
            const int moves_done = _model->getGameData().movePlayer(deltas[action - 10].first, deltas[action - 10].second);
            _graphicView->setPlayerDirection(action - 10); // change the player direction
            _graphicView->animatePlayerMove(old_grid, moves_done); // tell the view that it must animate the player movement
            if ((_model->getGameData().getPlayer().hasLost()) || (_model->getGameData().playerHasWon())) // if level is over
            {
                _graphicView->prepareMenu("playing_title");
                while (_graphicView->isAnimatingGame()) // SHOULD BE FIXED: game is paused until the end of the movement animation
                    _view->draw();
                if (_model->getGameData().playerHasWon()) // if player has won
                    _model->getGameData().addTimeLeftBonus(); // add bonus accordingly to time left
                else // else, animating explosion
                    _graphicView->animatePlayerExplosion();
                prevScreen();
                nextScreen(Screen::LEVEL_END); // end the level
            }
        }
    }   while (!action); // Wait for an action
}

void GraphicController::levelEnd()
{
    if (_model->getGameData().isCampaignFinished())
        _graphicView->prepareMenu("finished_title"); // You finished the campaign
    else if (!_model->getGameData().getPlayer().hasLost())
        _graphicView->prepareMenu("won_title"); // You have won
    else if (_model->getGameData().getPlayer().getScores().getLives() <= 0)
        _graphicView->prepareMenu("lost_title"); // You have lost
    else
        _graphicView->prepareMenu("fail_title"); // You have failed

    int action;
    do
    {
        _view->draw();
        action = _graphicView->getAction();
        if (action < 0)
            exit();
        else if (action == 1) // If the user wants to continue playing
        {
            _graphicView->stopAnimation();
            nextLevel();
            _graphicView->setPlayerDirection(4);
        }
        else if (action)
        {
            _graphicView->stopAnimation();
            const std::vector<std::pair<int, std::string>> highscores_list = _model->getGameData().loadScores();
            if ((_model->getGameData().getPlayer().getScores().getTotalScore() > 0)
             && ((static_cast<int>(highscores_list.size()) < GameData::max_stored_scores)
              || (_model->getGameData().getPlayer().getScores().getTotalScore() > highscores_list.back().first)))
                saveScoreDialog();
            prevScreen();
        }
    }   while (!action); // Wait for an action
}

void GraphicController::saveScoreDialog()
{
    _graphicView->preparePlayerNamePrompt("game_over");

    int action;
    do
    {
        _view->draw();
        action = _graphicView->getAction();
        if (action < 0)
            exit();
        else if (action == 1)
            _model->getGameData().endGame(_graphicView->getTextBoxTypedText());
        else if (action == 2)
            _model->getGameData().endGame(std::string());
    }   while (!action); // Wait for an action
}


void GraphicController::editor()
{
    // NOT IMPLEMENTED YET
    prevScreen();
}
