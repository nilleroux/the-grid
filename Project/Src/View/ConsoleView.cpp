/*! \file ConsoleView.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <string>

#include "ConsoleView.hpp"


ConsoleView::ConsoleView(const Model &model) : View(model)
{}


void ConsoleView::prepareMenu(const std::string &title, const std::vector<std::string> &choices, int selected)
{
    // Menu preparation simply gets texts to display
    _menuTitle = title;
    _menuChoices = choices;
    _selectedChoice = selected;
}


std::string ConsoleView::applyColor(const std::string &str, Color color, int mode)
{
    // Colors are put using weird symbols around the string; but it only works or Linux
#ifdef LINUX_CONSOLE_COLOR
    return "\E[3" + std::to_string(static_cast<int>(color)) + ';' + std::to_string(mode) + 'm' + str + "\E[m";
#else
    return str;
#endif
}


void ConsoleView::drawTitle() const
{
    // The title is displayed in yellow and underlined with tildas
    std::cout << applyColor(_model->getLabel(_menuTitle), Color::YELLOW) << std::endl
              << applyColor(std::string(Model::getStringLength(_model->getLabel(_menuTitle)), '~'), Color::YELLOW) << std::endl;
}

void ConsoleView::drawMenu() const
{
    // The menu is composed of choices with a number for each one
    int i = 1;
    for (const auto &chc : _menuChoices)
    {
        std::cout << i << ") " << _model->getLabel(chc);
        // Selected choice has a special text after it
        if (_selectedChoice == i - 1)
            std::cout << _model->getLabel("current");
        std::cout << std::endl;
        i++;
    }
}

void ConsoleView::drawScores() const
{
    // There is a full bunch of scores to draw during the game!
    const GameData &data = _model->getGameData();
    const std::string colon_label = _model->getLabel("colon");
    const std::vector<std::vector<std::pair<std::string, int>>> values_tags =
            { { { "total_score", data.getPlayer().getScores().getTotalScore() }, { "level", data.getLevelNum() },
                { "goal", data.getTarget() }, { "moves", data.getPlayer().getScores().getMoves() },
                { "level_score", data.getPlayer().getScores().getLevelScore() },
                { "lives", data.getPlayer().getScores().getLives() } },
              { { "bonus_quantity", data.getPlayer().getScores().getNbBonus() },
                { "time_bonus", static_cast<int>(data.getPlayer().getScores().getTimeBonus().count()) / 1000 },
                { "score_bonus", data.getPlayer().getScores().getScoreBonus() },
                { "lives_bonus", data.getPlayer().getScores().getLivesBonus() } } };
    std::vector<std::pair<int, int>> labels_widths(values_tags.size());
    int col_num, value_num, longest_col = 0, value;
    std::string label;
    // Structure is visible during the game: two columns (but it could work with more actually)

    for (col_num = 0; col_num < static_cast<int>(values_tags.size()); col_num++)
    {
        // We must find the longest column to know when we can stop writing lines
        if (longest_col < static_cast<int>(values_tags[col_num].size()))
            longest_col = values_tags[col_num].size();
        // And the longest labels and values are also needed to align everything correctly
        for (const auto &tag : values_tags[col_num])
        {
            if (labels_widths[col_num].first < static_cast<int>(Model::getStringLength(_model->getLabel(tag.first))))
                labels_widths[col_num].first = Model::getStringLength(_model->getLabel(tag.first));
            if (labels_widths[col_num].second < static_cast<int>(Model::getIntegerLength(tag.second)))
                labels_widths[col_num].second = Model::getIntegerLength(tag.second);
        }
    }

    // And now, we can write the lines
    for (value_num = 0; value_num < longest_col; value_num++)
    {
        // A score from each column at each line
        for (col_num = 0; col_num < static_cast<int>(values_tags.size()); col_num++)
        {
            // Columns are separated with vertical bars
            if (col_num > 0)
                std::cout << " | ";
            // If there is a value to display, it is aligned correctly with spaces
            if (value_num < static_cast<int>(values_tags[col_num].size()))
            {
                label = _model->getLabel(values_tags[col_num][value_num].first);
                value = values_tags[col_num][value_num].second;
                std::cout << std::string(labels_widths[col_num].first - Model::getStringLength(label), ' ') << label << colon_label
                          << std::string(labels_widths[col_num].second - Model::getIntegerLength(value), ' ') << value;
            }
            // Otherwise, space is filled with blanks
            else
                std::cout << std::string(labels_widths[col_num].first + labels_widths[col_num].second
                                       + Model::getStringLength(colon_label), ' ');
        }
        std::cout << std::endl;
    }

    // Time is treated seperately
    const int total_time_space = 30, elapsed_time = data.getElapsedTime().count(), time_limit = data.getTimeLimit().count(),
              filled_time_space = std::min(total_time_space, total_time_space * std::max(0, elapsed_time) / time_limit);
    // It is drawn as a bar filled with dashes, and with values
    std::cout << _model->getLabel("elapsed_time") << colon_label << std::string(filled_time_space, '-')
              << std::string(total_time_space - filled_time_space, ' ') << ' ' << elapsed_time / 1000 << " / "
              << time_limit / 1000 << std::endl;

    // If there is a time left bonus (at the end of the level), it is indicated here
    if (_model->getGameData().getPlayer().getScores().getTimeLeftBonus())
        std::cout << _model->getLabel("time_left_bonus") << colon_label
                  << _model->getGameData().getPlayer().getScores().getTimeLeftBonus() << std::endl;
    if (_model->getGameData().getPlayer().getScores().getCampaignBonus())
        std::cout << _model->getLabel("campaign_bonus") << colon_label
                  << _model->getGameData().getPlayer().getScores().getCampaignBonus() << std::endl;
}

void ConsoleView::drawControls() const
{
    // Game controls are labels
    std::cout << applyColor(_model->getLabel("controls") + _model->getLabel("colon"), Color::GREEN) << std::endl
              << _model->getLabel("console_controls") << std::endl;
}

void ConsoleView::draw()
{
    // Title is drawn first
    drawTitle();
    // And the rest depends on the current screen
    switch (_model->getTopScreen())
    {
        case Screen::GAME :
            // The game is filled with scores, the grid, a possible message, and controls
            drawScores();
            std::cout << std::endl << _model->getGameData().toString() << std::endl;
            if (!_model->getGameData().getLevelSolver().isSolvable())
                displayMessage("level_is_unsolvable");
            drawControls();
            break;
        case Screen::GAME_INIT :
            // Before the game, a hidden grid is shown
            std::cout << _model->getGameData().toString(true) << std::endl;
            break;
        case Screen::LEVEL_END :
            // At the end of the level, scores and the grid are displayed
            drawScores();
            std::cout << std::endl << _model->getGameData().toString() << std::endl;
            break;
        default : ;
    }
    // If there are choices, there is a menu
    drawMenu();
}

void ConsoleView::drawHighscores(const std::vector<std::pair<int, std::string>> &scores_list) const
{
    for (const auto &score : scores_list)
    {
        std::ostringstream score_buf;
        score_buf << score.first;
        std::cout << score.second << _model->getLabel("colon") << applyColor(score_buf.str(), Color::MAGENTA) << std::endl;
    }
    std::cout << std::endl;
}


void ConsoleView::displayMessage(const std::string &key) const
{
    // Special messages are shown as warnings
    std::cout << applyColor(_model->getLabel(key), Color::RED) << std::endl << std::endl;
}


int ConsoleView::askChoice(int last_choice) const
{
    int choice;
    std::string line;

    // If the number of choices are not customized, it is symply the number of elements in the menu
    if (last_choice < 0)
        last_choice = _menuChoices.size();

    do
    {
        // And we ask the user for a choice until we gets a valid one
        std::cout << applyColor(_model->getLabel("choice"), Color::WHITE) << ' ';
        std::getline(std::cin, line);
        std::istringstream input_buf(line);
        input_buf >> choice;
        // A good choice uses the full line that was typed
        if (std::cin.eof())
        {
            std::cerr << last_choice << std::endl;
            choice = last_choice;
        }
        else if (!input_buf.eof())
            choice = 0;
    }   while ((choice < 1) || (last_choice < choice));
    std::cout << std::endl;

    // Choice is returned with the first one being zero
    return choice - 1;
}

int ConsoleView::askInteger(const std::string &question) const
{
    int choice;
    std::string line;
    bool done = false;

    std::cout << applyColor(_model->getLabel(question), Color::WHITE, 3) << std::endl;

    while (!done)
    {
        std::cout << applyColor(_model->getLabel("choice"), Color::WHITE) << ' ';
        std::getline(std::cin, line);
        std::istringstream input_buf(line);
        input_buf >> choice;
        done = true;
        if (std::cin.eof())
        {
            std::cerr << '0' << std::endl;
            choice = 0;
        }
        else if (!input_buf.eof())
            done = false;
    }
    std::cout << std::endl;

    return choice;
}

std::string ConsoleView::askPlayerName(const std::string &question) const
{
    std::string name;
    std::cout << applyColor(_model->getLabel(question), Color::WHITE, 3) << std::endl;
    std::getline(std::cin, name);
    std::cout << std::endl;
    return name;
}
