var searchData=
[
  ['newgame',['newGame',['../class_game_data.html#abe0304354f38735d260c9c014e51edf0',1,'GameData::newGame(Difficulty diff=Difficulty::MEDIUM)'],['../class_game_data.html#adaf83530037a5dfa9779612d41aa777d',1,'GameData::newGame(int campaign_num)']]],
  ['next',['next',['../struct_solving_tree.html#a077f7afd1c25d9a6d75548cab7f24103',1,'SolvingTree']]],
  ['nextlevel',['nextLevel',['../class_controller.html#a83e415d8e8202db0c9708d0fd706690d',1,'Controller::nextLevel()'],['../class_game_data.html#a37f21acb73b6857cfee6dbefa4b582f2',1,'GameData::nextLevel()']]],
  ['nextscreen',['nextScreen',['../class_controller.html#a06dd7f71cbfcfa6923caa294df14b192',1,'Controller']]],
  ['none',['NONE',['../class_console_view.html#a8db5fd10f0e914ab942f05e39f3795c6ab50339a10e1de285ac99d4c3990b8693',1,'ConsoleView::NONE()'],['../_console_controller_8hpp.html#a21724820cc67afda28af7310157ea22cab50339a10e1de285ac99d4c3990b8693',1,'NONE():&#160;ConsoleController.hpp'],['../_model_8hpp.html#a64e169b6e0f42914937270604cc438edab50339a10e1de285ac99d4c3990b8693',1,'NONE():&#160;Model.hpp']]]
];
