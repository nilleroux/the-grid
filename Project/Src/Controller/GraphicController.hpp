/*! \file GraphicController.hpp
 * \brief The interface between the graphic view and the model.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_GRAPHICCONTROLLER_HPP
#define PPD_GRAPHICCONTROLLER_HPP


#include "Controller.hpp"
#include "../View/GraphicView.hpp"


/*! \class GraphicController
* \brief The interface between the graphic view and the model
*/
class GraphicController : public Controller
{
    protected :
        GraphicView* _graphicView; ///< The view associated to this controller

    public :
        // Object creation needs a model and a graphic view
        /*!
         *  \brief GraphicController constructor
         *  \param[in] model : The model to associate to this Controller
         *  \param[in] view : The view to associate with this Controller
         */
        GraphicController(Model &model, GraphicView &view);

    private :
        /*!
         *  \brief Handles the main menu.
         */
        virtual void menu() override;
        /*!
         *  \brief Handles the options menu screens.
         */
        virtual void options() override;

        /*!
         *  \brief Handles the highscores screen.
         */
        virtual void highscores() override;
        /*!
         *  \brief Handles the highscores table.
         *  \param[in] The difficulty level (HARD+1 if campaign)
         */
        virtual void highscoresTable(int choice);

        /*!
         *  \brief Campaign selection screen.
         */
        virtual void gameInit() override;
        /*!
         *  \brief Handles the start of a new campaign.
         */
        virtual void gameSelectCampaign() override;
        /*!
         *  \brief Game screen.
         */
        virtual void game() override;
        /*!
         *  \brief Handles the end of a level.
         */
        virtual void levelEnd() override;
        /*!
         *  \brief Save score dialog screen.
         */
        virtual void saveScoreDialog();

        /*!
         *  \brief Handles the level editor.
         *  \todo Do the level editor.
         */
        virtual void editor() override;
};


#endif
