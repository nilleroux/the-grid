/*! \file main.cpp
 *  \brief The main file, launches the main menu and calls the controller as long as needed.
 *  \author { Sylvain Chiron, Nils Le Roux }
 */

#include "Model/Model.hpp"
#include "View/ConsoleView.hpp"
#include "Controller/ConsoleController.hpp"
#include "View/GraphicView.hpp"
#include "Controller/GraphicController.hpp"


/**
 * \fn int main(int argc, const char* argv[])
 * \brief Main
 *
 * \param[in] argc : amount of params
 * \param[in] argv : params values
 * \return Execution status
 */
int main(int argc, const char* argv[])
{
    Model model;
    View* view;
    Controller* controller;

    int arg_num;
    bool console = false;
    for (arg_num = 1; arg_num < argc; arg_num++)
        if (std::string(argv[arg_num]) == std::string("-c"))
            console = true;
        else if (std::string(argv[arg_num]) == std::string("-g"))
            console = false;

    // Program arguments determine which view we are using
    if (console)
    {
        ConsoleView* console_view = new ConsoleView(model);
        controller = new ConsoleController(model, *console_view);
        view = console_view;
    }
    else
    {
        GraphicView* graphic_view = new GraphicView(model);
        controller = new GraphicController(model, *graphic_view);
        view = graphic_view;
    }

    model.pushScreen(Screen::MENU);
    while (model.getTopScreen() != Screen::NONE)
        controller->act();

    delete controller;
    delete view;

    return 0;
}
