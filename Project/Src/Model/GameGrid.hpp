/*! \file GameGrid.hpp
 * \brief Originally intended to be a wrapper for a matrix of cells (so just basic functions to manage a two-dimensional array),
 *        but finally also got memory management methods.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_MODEL_GAMEGRID_HPP
#define PPD_MODEL_GAMEGRID_HPP


#include <vector>

#include "GameCell.hpp"


/*! \class GameGrid
* \brief A representation of a grid.
*/
class GameGrid
{
        int _w, ///< Grid width
            _h; ///< Grid height
        std::vector<GameCell*> _cells; ///< Cells of the grid

    public :
        /*!
         *  \brief GameGrid constructor
         *  \param[in] w : the width of the grid
         *  \param[in] h : the height of the grid
         */
        GameGrid(int w = 0, int h = 0);
        /*!
         *  \brief Copy constructor of GameGrid
         *  \param[in] grid : the GameGrid to copy
         */
        GameGrid(const GameGrid &grid);
        /*!
         *  \brief GameGrid destructor
         */
        ~GameGrid();

        /*!
         *  \brief Width getter
         *  \return The grid width
         */
        int getWidth() const;
        /*!
         *  \brief Height getter
         *  \return The grid height
         */
        int getHeight() const;
        /*!
         *  \brief Total cells
         *  \return The total amount of cells
         */
        int getTotalCells() const;
        /*! 
         *  \brief Checks whether the given coordinates lays on the grid or not
         *  \param[in] x : X-Coordinate
         *  \param[in] y : Y-Coordinate
         *  \return true : given coordinates exist in grid, false : otherwise
         */
        bool isOnGrid(int x, int y) const;

        /*! 
         *  \brief GameGrid assignement operator
         *  \param[in] grid : The grid to assign to the current grid
         *  \return const reference to the assigned grid
         */
        GameGrid &operator=(const GameGrid &grid);
        /*! 
         *  \brief Allows to access to the "cell" index of the cells vector.
         *  \param[in] cell : the index of the cell to access
         *  \return The cell
         */
        GameCell* &operator[](int cell);
        /*! 
         *  \brief Allows to access to the "cell" index of the cells vector.
         *  \param[in] cell : the index of the cell to access
         *  \return The cell
         */
        const GameCell* operator[](int cell) const;
        /*! 
         *  \brief Allows to access to the (x, y) coordinate of the cells vector.
         *  \param[in] x : X-coordinate
         *  \param[in] y : Y-coordinate
         *  \return The cell
         */
        GameCell* &operator()(int x, int y);
        /*! 
         *  \brief Allows to access to the (x, y) coordinate of the cells vector.
         *  \param[in] x : X-coordinate
         *  \param[in] y : Y-coordinate
         *  \return The cell
         */
        const GameCell* operator()(int x, int y) const;

        /*! 
         *  \brief Resizes the grid.
         *  \param[in] w : The new width
         *  \param[in] h : The new height
         */
        void resize(int w, int h);
        /*! 
         *  \brief Mixes up the grid randomly.
         */
        void shuffle();

    private :
        /*! 
         *  \brief Frees the memory of each cell in the grid.
         */
        void freeCells();
        /*! 
         *  \brief Clones cells from another grid.
         *  \param grid : the grid which contains the cells to clone
         */
        void cloneCells(const GameGrid &grid);
};


#endif
