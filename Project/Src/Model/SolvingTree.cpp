/*! \file SolvingTree.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include "SolvingTree.hpp"


SolvingTree::SolvingTree(const Movement &move, SolvingTree* prev) : move(move), prev(prev)
{}
