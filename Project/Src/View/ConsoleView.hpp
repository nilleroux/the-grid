/*! \file ConsoleView.hpp
 *  \brief A simple but very rich view using a console window.
 *  \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_CONSOLEVIEW_HPP
#define PPD_CONSOLEVIEW_HPP


#include <vector>
#include <string>

#include "View.hpp"


/*! \class ConsoleView
* \brief A simple but very rich view using a console window. 
*/
class ConsoleView : public View
{
        /// Colors which can be used in the console view
        enum class Color
        {
            NONE,       ///< No color
            RED,        ///< Red color
            GREEN,      ///< Green color
            YELLOW,     ///< Yellow color
            BLUE,       ///< Blue color
            MAGENTA,    ///< Magenta color
            CYAN,       ///< Cyan color
            WHITE       ///< White color
        };

        // Drawing needs: a title, choices, and possibly a selected choice
        std::string _menuTitle; ///< The menu title
        std::vector<std::string> _menuChoices; ///< Menu available choices
        int _selectedChoice; ///< The choice selected in the menu

    public :
        /*! 
         *  \brief ConsoleView constructor
         *  \param[in] model : The model is needed to construct the view
         */
        ConsoleView(const Model &model);

        /*! 
         *  \brief The menu must be prepared before being drawn
         *  \param[in] title : The menu title
         *  \param[in] choices : The available choices in the menu
         *  \param[in] selected : The default choice selected
         */
        void prepareMenu(const std::string &title, const std::vector<std::string> &choices = {}, int selected = -1);

    private :
        /*! 
         *  \brief Applies a color to a string. (Only work on Linux shells)
         *  \param[in] str : The string on which the color should be applied
         *  \param[in] color : The color to apply to the string
         *  \param[in] mode : The color mode
         *  \return The colored string
         */
        static std::string applyColor(const std::string &str, Color color, int mode = 1);

        // A menu is drawn in several parts
        /*! 
         *  \brief Draws the title
         */
        virtual void drawTitle() const;
        /*! 
         *  \brief Draws the menu
         */
        virtual void drawMenu() const;
        /*! 
         *  \brief Draws the scores
         */
        virtual void drawScores() const;
        /*! 
         *  \brief Draws the controls
         */
        virtual void drawControls() const;
    public :
        /*! 
         *  \brief Draws everything. Entry point of the view.
         */
        virtual void draw() override;
        /*! 
         *  \brief Draws the highscores
         *  \param[in] scores_list : a vector containing scores and names
         */
        virtual void drawHighscores(const std::vector<std::pair<int, std::string>> &scores_list) const;

        /*! 
         *  \brief Diplays a given message on the screen
         *  \param[in] key : string to display
         */
        virtual void displayMessage(const std::string &key) const;

        /*! 
         *  \brief This function lets the user choose an option
         *  \param[in] last_choice : the previously selected value
         *  \return The selected value
         */
        virtual int askChoice(int last_choice = -1) const;
        /*! 
         *  \brief Asks for a value until it is an integer
         *  \param[in] question : the question to print before asking for a integer
         *  \return The inputed integer
         */
        virtual int askInteger(const std::string &question) const;
        /*! 
         *  \brief Asks for the player name
         *  \param[in] question : the question to print before asking for the player name 
         *  \return The player name
         */
        virtual std::string askPlayerName(const std::string &question) const;
};


#endif
