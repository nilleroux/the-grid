/*! \file GameNormalCell.hpp
 *  \brief For the numbered cells in the grid.
 *  \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_MODEL_GAMENORMALCELL_HPP
#define PPD_MODEL_GAMENORMALCELL_HPP


#include "GameCell.hpp"


/*! \class GameNormalCell
* \brief This class describes Normal Cells, that is to say valued cells (the most common type of cells).
*/
class GameNormalCell : public GameCell
{
        int _value; ///< The value of the cell

    public :
        /*! 
         *  \brief GameNormalCell constructor
         *  \param[in] value : the value of the cell
         */
        GameNormalCell(int value = 0);
        
        /*!
         *  \brief Allows a Cell to be cloned
         *  \return a pointer to the cloned cell.
         */
        virtual GameCell* clone() const override;

        /*!
         *  \brief toString method for GameNormalCell.
         *  \param[in] player : the cell state of the player (to draw it correctly)
         *  \return A string representation of the cell.
         */
        virtual std::string toString(PlayerCellState player = PlayerCellState::ELSEWHERE) const override;

    protected :
        /*!
         *  \brief Applies a crossing to a player (updates his state and scores)
         *  \param[in] player : the player on which crossing must be applied
         */
        virtual void applyCrossing(Player &player) override;
        /*!
         *  \brief Uncrosses the cell and update player state and scores.
         *  \param[in] player : the player on which crossing must be applied
         */
        virtual void undoCrossing(Player &player) override;

    public :
        /*!
         *  \brief Cell value getter
         *  \return The cell value
         */
        int getValue() const;
};


#endif
