#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Model tests
#include <boost/test/unit_test.hpp>

#include "../Src/Model/Model.hpp"
#include "../Src/Model/GameData.hpp"
#include "../Src/Model/GameCell.hpp"
#include "../Src/Model/GameBonusCell.hpp"
#include "../Src/Model/GameBombCell.hpp"
#include "../Src/Model/GameNormalCell.hpp"


BOOST_AUTO_TEST_CASE(GAMEDATA_TEST)
{

    GameData gd;
    const GameGrid &gg = gd.getGrid();
    const GameNormalCell* gameNormalCell;
    const GameBonusCell* gameBonusCell;
    const GameTimeBonusCell* gameTimeBonusCell;
    const GameScoreBonusCell* gameScoreBonusCell;
    const GameBombCell* gameBombCell;
    int i, level, diff_value;

    // Testing for each difficulty level (0 = EASY; 1 = MEDIUM; 2 = HARD)
    for (diff_value = 0; diff_value <= 2; ++diff_value)
    {
        int target = diff_value * 10; // Target default value which will be increased each time a level is passed
        gd.newGame(static_cast<Difficulty>(diff_value)); // Starting a new game
        for (level = 1; level <= 50; ++level) // The grids will be validated for 50 levels for each difficulty level
        {
            // Setting up values that we are supposed to reach
            const int min_value = diff_value + 1, max_value = 5 + level / 6,
                grid_width = 16 + level / 2, grid_height = 16 + level / 3,
                bombs = 9 + (diff_value + 1) * level / 2, bonuses = grid_width / 2 - diff_value,
                score_bonuses = bonuses * 2 / 5,
                time_bonuses = bonuses * 4 / 5 - score_bonuses;
            // Setting up vars that will allow us to count
            int nbombs = 0,
                nbonuses = 0,
                nscore_bonuses = 0,
                ntime_bonuses = 0;
            bool playerCellFound = false; // We need to know if the player cell was found

            target += (level > 5) ? 5 : 10; // Increasing the target for each level

            BOOST_CHECK(gd.getTarget() == target); // Checking target
            BOOST_CHECK(gg.getWidth() == grid_width); // Checking grid width
            BOOST_CHECK(gg.getHeight() == grid_height); // Checking grid height

            for (i = 0; i < gg.getTotalCells(); ++i) // For each cell on the grid, we can count cells and their type.
            {
                if ((gameNormalCell = dynamic_cast<const GameNormalCell*>(gg[i])))
                {
                    BOOST_CHECK(
                            (gameNormalCell->getValue() == 0 && !playerCellFound) // Checking that it is not the player's cell
                            || (min_value <= gameNormalCell->getValue() && gameNormalCell->getValue() <= max_value) // and that normal cell's value is correct.
                    );

                    if (gameNormalCell->getValue() == 0) // If it was the player's cell
                        playerCellFound = true;

                    if ((gameBonusCell = dynamic_cast<const GameBonusCell*>(gg[i])))
                    {
                        ++nbonuses;

                        if ((gameScoreBonusCell = dynamic_cast<const GameScoreBonusCell*>(gg[i])))
                            ++nscore_bonuses;
                        else if ((gameTimeBonusCell = dynamic_cast<const GameTimeBonusCell*>(gg[i])))
                            ++ntime_bonuses;
                    }
                }
                else if ((gameBombCell = dynamic_cast<const GameBombCell*>(gg[i])))
                    ++nbombs;

            }

            BOOST_CHECK(playerCellFound); // We need to find player's cell once
            // Then, we can check if there is the correct amount of each type of cell in the grid.
            BOOST_CHECK(bonuses == nbonuses);
            BOOST_CHECK(score_bonuses == nscore_bonuses);
            BOOST_CHECK(time_bonuses == ntime_bonuses);
            BOOST_CHECK(bombs == nbombs);

            gd.nextLevel();

        }
    }

}

BOOST_AUTO_TEST_CASE(SCREENS_TEST)
{
    Model model;
    model.pushScreen(Screen::MENU);
    BOOST_CHECK(model.getTopScreen() == Screen::MENU);
    model.pushScreen(Screen::GAME_INIT);
    model.pushScreen(Screen::GAME);
    BOOST_CHECK(model.getTopScreen() == Screen::GAME);
    model.popScreen();
    BOOST_CHECK(model.getTopScreen() == Screen::GAME_INIT);
    model.popScreen();
    model.popScreen(); // Poping too much screens must lead to no screen poped.
    BOOST_CHECK(model.getTopScreen() == Screen::NONE);
    model.popScreen();
    BOOST_CHECK(model.getTopScreen() == Screen::NONE);
}

BOOST_AUTO_TEST_CASE(OPTIONS_TEST)
{
    Model model;
    // Checking direct options setter
    model.setSpecificOption("difficulty", "easy");
    BOOST_CHECK(model.getDifficulty() == Difficulty::EASY);
    model.setSpecificOption("difficulty", "medium");
    BOOST_CHECK(model.getDifficulty() == Difficulty::MEDIUM);
    model.setSpecificOption("difficulty", "hard");
    BOOST_CHECK(model.getDifficulty() == Difficulty::HARD);

    // Checking Difficulty option setter
    model.changeDifficulty(Difficulty::EASY);
    BOOST_CHECK(model.getDifficulty() == Difficulty::EASY);
    model.changeDifficulty(Difficulty::MEDIUM);
    BOOST_CHECK(model.getDifficulty() == Difficulty::MEDIUM);
    model.changeDifficulty(Difficulty::HARD);
    BOOST_CHECK(model.getDifficulty() == Difficulty::HARD);
}

BOOST_AUTO_TEST_CASE(LANGS_TEST)
{
    Model model;

    // Checking language setter
    model.changeLanguage("english");
    BOOST_CHECK(model.getOption("language") == "english");
    BOOST_CHECK(model.getLabel("play") == "Play"); // Checking that labels are loaded correctly
    model.changeLanguage("french"); // Checking that changing language again changes the options and the language correctly
    BOOST_CHECK(model.getOption("language") == "french");
    BOOST_CHECK(model.getLabel("play") == "Jouer");
}

BOOST_AUTO_TEST_CASE(STRINGLENGTH_TEST)
{
    BOOST_CHECK(Model::getStringLength("") == 0);
    BOOST_CHECK(Model::getStringLength("foo") == 3);
    BOOST_CHECK(Model::getStringLength("foo bar") == 7);
    BOOST_CHECK(Model::getStringLength("123foo") == 6);
    BOOST_CHECK(Model::getStringLength("éè,;ù©&") == 7); // UTF-8 string
}

BOOST_AUTO_TEST_CASE(INTLENGTH_TEST)
{
    BOOST_CHECK(Model::getIntegerLength(0) == 1);
    BOOST_CHECK(Model::getIntegerLength(203) == 3);
    BOOST_CHECK(Model::getIntegerLength(-20) == 3); // neg
    BOOST_CHECK(Model::getIntegerLength(-203) == 4); // neg
    BOOST_CHECK(Model::getIntegerLength(9284756) == 7);
}

BOOST_AUTO_TEST_CASE(PLAYER_TEST)
{
    Player p;
    p.lose();
    BOOST_CHECK(p.hasLost());
    p.resurrect();
    BOOST_CHECK(!p.hasLost());
    p.setPos(17, 32);
    BOOST_CHECK(p.getX() == 17 && p.getY() == 32);
}

BOOST_AUTO_TEST_CASE(GRID_TESTS)
{
    GameData gd;
    const Player &player = gd.getPlayer();
    gd.newGame(1);
    BOOST_CHECK(player.getX() == 1 && player.getY() == 2 && !gd.playerHasWon() && !player.hasLost());
    gd.movePlayer(0, 1);
    gd.movePlayer(1, 0);
    BOOST_CHECK(gd.getGrid()(1, 2)->getCrossed());
    BOOST_CHECK(player.getX() == 2 && player.getY() == 2 && !gd.playerHasWon() && !player.hasLost());
    gd.movePlayer(0, -1);
    gd.movePlayer(0, -1);
    BOOST_CHECK(!gd.playerHasWon() && player.hasLost());

    gd.newGame(1);
    gd.movePlayer(0, 1);
    BOOST_CHECK(player.getX() == 1 && player.getY() == 2 && !gd.playerHasWon() && !player.hasLost());
    gd.movePlayer(1, 0);
    BOOST_CHECK(player.getX() == 2 && player.getY() == 2 && !gd.playerHasWon() && !player.hasLost());

    gd.newGame(1);
    gd.nextLevel();
    gd.movePlayer(0, 1);
    BOOST_CHECK(player.getX() == 0 && player.getY() == 3 && !gd.playerHasWon() && player.hasLost());

    int lives;
    gd.newGame(1);
    gd.nextLevel();
    gd.nextLevel();
    lives = player.getScores().getLives();
    gd.movePlayer(0, 1);
    BOOST_CHECK(player.getScores().getLivesBonus() == 1 && lives == (player.getScores().getLives()-1));
}


BOOST_AUTO_TEST_CASE(CELLS_TEST)
{
    Player p;

    GameBombCell bomb_cell;
    GameScoreBonusCell score_bonus_cell(3, 5);
    GameTimeBonusCell time_bonus_cell(2, std::chrono::seconds(3));
    GameLifeBonusCell life_bonus_cell(4);
    GameNormalCell normal_cell(3);

    GameCell* cell_clone;
    GameNormalCell* normal_cell_clone;
    cell_clone = normal_cell.clone(); // Cloning the cell
    normal_cell_clone = dynamic_cast<GameNormalCell*>(cell_clone);
    BOOST_CHECK(normal_cell_clone != NULL); // Cast MUST work
    if (normal_cell_clone)
        BOOST_CHECK(normal_cell_clone->getValue() == normal_cell.getValue() // checking apparent value
                && normal_cell_clone->getCrossed() == normal_cell.getCrossed()
                && normal_cell_clone != &normal_cell);
    delete cell_clone; // freeing the cloned cell
    cell_clone = NULL;
    normal_cell_clone = NULL;

    BOOST_CHECK(bomb_cell.toString(PlayerCellState::DEADONCELL) == "$@$"
            && bomb_cell.toString(PlayerCellState::ONCELL) == "$@$"
            && bomb_cell.toString(PlayerCellState::ELSEWHERE) == "<@>");
    bomb_cell.cross(p);
    BOOST_CHECK(bomb_cell.getCrossed()
            && bomb_cell.toString(PlayerCellState::DEADONCELL) == "$@$"
            && bomb_cell.toString(PlayerCellState::ONCELL) == "$@$"
            && bomb_cell.toString(PlayerCellState::ELSEWHERE) == "x@x");

    BOOST_CHECK(score_bonus_cell.toString(PlayerCellState::ONCELL) == "$-$"
            && score_bonus_cell.toString(PlayerCellState::DEADONCELL) == "$X$"
            && score_bonus_cell.toString(PlayerCellState::ELSEWHERE) == "-3-");
    score_bonus_cell.cross(p);
    BOOST_CHECK(score_bonus_cell.toString(PlayerCellState::ONCELL) == "$-$"
            && score_bonus_cell.toString(PlayerCellState::DEADONCELL) == "$X$"
            && score_bonus_cell.toString(PlayerCellState::ELSEWHERE) == "-X-");

    BOOST_CHECK(time_bonus_cell.toString(PlayerCellState::ONCELL) == "$-$"
            && time_bonus_cell.toString(PlayerCellState::DEADONCELL) == "$X$"
            && time_bonus_cell.toString(PlayerCellState::ELSEWHERE) == "-2-");
    time_bonus_cell.cross(p);
    BOOST_CHECK(time_bonus_cell.toString(PlayerCellState::ONCELL) == "$-$"
            && time_bonus_cell.toString(PlayerCellState::DEADONCELL) == "$X$"
            && time_bonus_cell.toString(PlayerCellState::ELSEWHERE) == "-X-");

    BOOST_CHECK(life_bonus_cell.toString(PlayerCellState::ONCELL) == "$-$"
            && life_bonus_cell.toString(PlayerCellState::DEADONCELL) == "$X$"
            && life_bonus_cell.toString(PlayerCellState::ELSEWHERE) == "-4-");
    life_bonus_cell.cross(p);
    BOOST_CHECK(life_bonus_cell.toString(PlayerCellState::ONCELL) == "$-$"
            && life_bonus_cell.toString(PlayerCellState::DEADONCELL) == "$X$"
            && life_bonus_cell.toString(PlayerCellState::ELSEWHERE) == "-X-");

    BOOST_CHECK(normal_cell.toString(PlayerCellState::ONCELL) == "$$$"
            && normal_cell.toString(PlayerCellState::DEADONCELL) == "$X$"
            && normal_cell.toString(PlayerCellState::ELSEWHERE) == " 3 ");
    normal_cell.cross(p);
    BOOST_CHECK(normal_cell.toString(PlayerCellState::ONCELL) == "$$$"
            && normal_cell.toString(PlayerCellState::DEADONCELL) == "$X$"
            && normal_cell.toString(PlayerCellState::ELSEWHERE) == "xXx");
}

BOOST_AUTO_TEST_CASE(LEVEL_SOLVER)
{
    GameData gd;
    const LevelSolver &level_solver = gd.getLevelSolver();

    gd.newGame(1);
    gd.nextLevel(); // unsolvable level
    BOOST_CHECK(!level_solver.isSolvable());

    gd.nextLevel();
    gd.nextLevel(); // solvable level
    BOOST_CHECK(level_solver.isSolvable());
}
