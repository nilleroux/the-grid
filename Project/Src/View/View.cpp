/*! \file View.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include "View.hpp"


View::View(const Model &model) : _model(&model)
{}

View::~View()
{}
