/*! \file LevelSolver.hpp
 * \brief A class to know at every moment if a level is solvable.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_LEVELSOLVER_HPP
#define PPD_LEVELSOLVER_HPP


#include "Movement.hpp"
#include "SolvingTree.hpp"


class GameData;

/*! \class LevelSolver
* \brief This class allows to check if a level is possible to finish. It also memorises the move of the player
*        in order to be able to undo his moves.
*/
class LevelSolver
{
        GameData* _level; ///< LevelSolver level data
        SolvingTree _tree; ///< The start of the SolvingTree
        SolvingTree* _currentBranch; ///< Branch on which the player currently is.

    public :
        /*! 
         *  \brief LevelSolver constructor
         *  \param[in] level : The GameData corresponding to the current level
         */
        LevelSolver(GameData* level = NULL);
        /*! 
         *  \brief LevelSolver copy constructor
         *  \param[in] level_solver : The level_solver to copy
         */
        LevelSolver(const LevelSolver &level_solver);

        /*! 
         *  \brief LevelSolver assignement operator
         *  \param[in] level_solver : The level_solver to copy
         */
        LevelSolver &operator=(const LevelSolver &level_solver);

        /*!
         *  \brief Gives the last movement that was registered
         *  \return The movement (empty if none was done)
         */
        Movement getLastMovement() const;

    private :
        /*! 
         *  \brief Checks if the level is possible to finish in a given situation
         *  \param[in] solving_tree : the solving tree corresponding to the current situation
         *  \param[in] moves_done : the amount of moves already done
         *  \return true : the level is possible to finish, false : otherwise
         */
        bool isSolvable(const SolvingTree &solving_tree, int moves_done) const;
    public :
        /*! 
         *  \brief Checks if the GameData level is possible to finish
         *  \return true : the level is possible to finish, false : otherwise
         */
        bool isSolvable() const;

    private :
        /*! 
         *  \brief Fills a given tree with next moves available
         *  \param[in] solving_tree : The tree to fill
         */
        void fillTree(SolvingTree &solving_tree);
    public :
         /*! 
         *  \brief Fills the solving tree.
         */
        void fillTree();

        /*!
         *  \brief Updates the current branch to a newer one.
         *  \param[in] dx : The X direction
         *  \param[in] dy : The Y direction
         *  \return The amount of moves
         */
        int goToBranch(int dx, int dy);
        /*!
         *  \brief Update the current branch to the previous one
         *  \return The cancelled movement (empty if none was cancelled)
         */
        Movement comeBackFromBranch();
};


#endif
