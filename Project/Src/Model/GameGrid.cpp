/*! \file GameGrid.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include <algorithm>

#include "GameGrid.hpp"


GameGrid::GameGrid(int w, int h) : _w(w), _h(h), _cells(w * h)
{}

GameGrid::GameGrid(const GameGrid &grid)
{
    cloneCells(grid);
}

GameGrid::~GameGrid()
{
    freeCells();
}


int GameGrid::getWidth() const
{
    return _w;
}

int GameGrid::getHeight() const
{
    return _h;
}

int GameGrid::getTotalCells() const
{
    return _w * _h;
}

bool GameGrid::isOnGrid(int x, int y) const
{
    return ((0 <= x) && (x < _w) && (0 <= y) && (y < _h));
}


GameGrid &GameGrid::operator=(const GameGrid &grid)
{
    if (this != &grid)
    {
        freeCells();
        cloneCells(grid);
    }

    return *this;
}

GameCell* &GameGrid::operator[](int cell)
{
    return _cells[cell];
}

const GameCell* GameGrid::operator[](int cell) const
{
    return _cells[cell];
}

GameCell* &GameGrid::operator()(int x, int y)
{
    return _cells[x * _h + y];
}

const GameCell* GameGrid::operator()(int x, int y) const
{
    return _cells[x * _h + y];
}


void GameGrid::resize(int w, int h)
{
    freeCells();

    _w = w;
    _h = h;
    _cells.resize(w * h);
}

void GameGrid::shuffle()
{
    std::random_shuffle(_cells.begin(), _cells.end());
}


void GameGrid::freeCells()
{
    _w = _h = 0;

    for (GameCell* cell : _cells)
        if (cell)
            delete cell;

    _cells.clear();
}

void GameGrid::cloneCells(const GameGrid &grid)
{
    _w = grid._w;
    _h = grid._h;
    _cells.reserve(_w * _h);

    for (GameCell* cell : grid._cells)
        _cells.push_back(cell->clone());
}
