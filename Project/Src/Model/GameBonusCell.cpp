/*! \file GameBonusCell.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include <sstream>
#include <iomanip>

#include "GameBonusCell.hpp"


GameBonusCell::GameBonusCell(int value) : GameNormalCell(value)
{}

std::string GameBonusCell::toString(PlayerCellState player) const
{
    if (player == PlayerCellState::ONCELL)
        return "$-$";
    else if (player == PlayerCellState::DEADONCELL)
        return "$X$";
    else if (getCrossed())
        return "-X-";
    std::ostringstream buf;
    buf << std::setfill('-') << std::setw(2) << getValue() << '-';
    return buf.str();
}

void GameBonusCell::applyCrossing(Player &player)
{
    GameNormalCell::applyCrossing(player);
    applyBonus(player.getScores());
}
void GameBonusCell::undoCrossing(Player &player)
{
    GameNormalCell::undoCrossing(player);
    undoBonus(player.getScores());
}


GameTimeBonusCell::GameTimeBonusCell(int value, const std::chrono::milliseconds &time_bonus)
        : GameBonusCell(value), _timeBonus(time_bonus)
{}

GameCell* GameTimeBonusCell::clone() const
{
    return new GameTimeBonusCell(*this);
}

void GameTimeBonusCell::applyBonus(ScoresList &scores)
{
    scores.pickTimeBonus(_timeBonus);
}
void GameTimeBonusCell::undoBonus(ScoresList &scores)
{
    scores.undoTimeBonus(_timeBonus);
}


GameScoreBonusCell::GameScoreBonusCell(int value, int score_bonus) : GameBonusCell(value), _scoreBonus(score_bonus)
{}

GameCell* GameScoreBonusCell::clone() const
{
    return new GameScoreBonusCell(*this);
}

void GameScoreBonusCell::applyBonus(ScoresList &scores)
{
    scores.pickScoreBonus(_scoreBonus);
}
void GameScoreBonusCell::undoBonus(ScoresList &scores)
{
    scores.undoScoreBonus(_scoreBonus);
}


GameLifeBonusCell::GameLifeBonusCell(int value) : GameBonusCell(value)
{}

GameCell* GameLifeBonusCell::clone() const
{
    return new GameLifeBonusCell(*this);
}

void GameLifeBonusCell::applyBonus(ScoresList &scores)
{
    scores.pickLifeBonus();
}
void GameLifeBonusCell::undoBonus(ScoresList &scores)
{
    scores.undoLifeBonus();
}
