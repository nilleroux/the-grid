/*! \file GraphicView.hpp
 *  \brief A Graphic view using the SFML Library
 *  \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_GRAPHICVIEW_HPP
#define PPD_GRAPHICVIEW_HPP


#include <vector>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "View.hpp"
#include "GraphicElement.hpp"


/*! \class GraphicView
* \brief A Graphic view using the SFML Library.
*/
class GraphicView : public View
{
        static const int tile_w,              ///< Title width
                         tile_h,              ///< Title height
                         buttons_width,       ///< Buttons width
                         buttons_height,      ///< Buttons height
                         explosion_frame_w,   ///< Explosion frame width
                         explosion_frame_h;   ///< Explosion frame height
        static const sf::FloatRect grid_rect; ///< Rectangle in which the grid is drawn

        // SFML string methods
        /*!
         *  \brief Allows to draw UTF-8 strings
         *  \param[in] text : The UTF-8 string to draw
         *  \return The usable-for-drawing string
         */
        static sf::String getDrawableText(const std::string &utf8_text);
        /*!
         *  \brief Gives a universal height for strings
         *  \param[in] text : The drawable text
         *  \return The height value
         */
        static float getTextHeight(const sf::Text &text);

        // Loaded ressources
        std::map<std::string, sf::Texture> _loadedImages; ///< SFML textures (images)
        std::map<std::string, sf::Font> _loadedFonts;   ///< SFML fonts
        std::map<std::string, sf::Music> _loadedMusics; ///< SFML musics
        sf::Image _timelineColors; ///< Image containing the colors of the timeline

        sf::RenderWindow _renderWindow; ///< SFML render window
        sf::View _windowView; ///< SFML view (region shown on the screen)

        std::string _menuTitle; ///< The current menu title
        std::vector<GraphicButton> _menuButtons;    ///< Vector of each buttons to draw
        std::vector<GraphicSelector> _menuSelectors;    ///< Vector of each graphic selector to draw
        GraphicTextBox _menuTextBox;     ///< User typed value (especially when the player must enter his name)

        std::string _currentTrack; ///< String key of the currently played track (set even if music is off)
        int _highscoreScreen;   ///< -1 if not highscore screen, 0 if random mode scores, 1 if campaign mode scores
        std::vector<std::pair<int, std::string>> _highscoresList; ///< The vector of highscores to draw (e.g.: 255 fifi33)
        int _playerDirection; ///< Player direction on the grid (to be able to draw the correct sprite)

        int _animationType; ///< 0: no current animation, 1: player is moving, 2: player is unmoving, 3: player is exploding
        sf::Clock _animationClock; ///< Clock for any animation
        GameGrid _oldGrid; ///< Grid state before player's movement
        int _playerMoves; ///< Number of cells the player must cross during the animation

    public :
        /*!
         *  \brief GraphicView constructor
         *  \param[in] model : The model is needed to construct the view
         */
        GraphicView(const Model &model);
        /*!
         *  \brief GraphicView destructor
         */
        virtual ~GraphicView() override;

        /*!
         *  \brief Creates the window with the given properties
         *  \param[in] fullscreen : Create a fullscreen window
         *  \param[in] video_mode : Video mode to use for the window
         */
        void createWindow(bool fullscreen = false, const sf::VideoMode &video_mode = { 800, 600 });

        /*!
         *  \brief Allows to know if an animation is currently shown
         *  \return true if an animation is being displayed, false otherwise
         */
        bool isAnimatingGame() const;
        /*!
         *  \brief Handles SFML events and buttons/selectors
         *  \return -1 for a closing event, 0 if nothing selected or the clicked button id
         */
        int getAction();
        /*!
         *  \brief Allows to get selected choice in a GraphicSelector
         *  \param[in] selector_num : The selector index in _menuSelectors
         *  \return The selected choice
         */
        int getSelectedChoice(int selector_num) const;
        /*!
         *  \brief Allows to get current textbox text.
         *  \return The typed text
         */
        std::string getTextBoxTypedText() const;

    private :
        /*!
         *  \brief Adds a GraphicButton to _menuButtons
         *  \param[in] id : The button id
         *  \param[in] position : The button position on the screen
         *  \param[in] label : Label key of the button
         *  \param[in] text_size : The text size
         *  \param[in] sprite_type : 0 if there is no hover image in the sprite, 1 if there is one.
         */
        void addMenuButton(int id, const sf::Vector2f &position, const std::string &label = std::string(), float text_size = 24.f,
                           int sprite_type = 0);
        /*!
         *  \brief Adds a MenuSelector to _menuSelectors
         *  \param[in] selected_option : Initial option
         *  \param[in] options : Available options
         *  \param[in] position : Selector position on the screen (centered)
         *  \param[in] options_are_labels : Flag to know if the options need to be translated for display
         *  \param[in] description : Selector description label
         *  \param[in] desc_color : Description text color
         *  \param[in] color : Selector text color
         *  \param[in] text_size : Selector text size
         */
        void addMenuSelector(int selected_option, const std::vector<std::string> &options, const sf::Vector2f &position,
                             bool options_are_labels = true, const std::string &description = std::string(),
                             const sf::Color &desc_color = { 224, 224, 224 }, const sf::Color &color = { 24, 192, 48 },
                             const sf::Color &hover_color = { 36, 255, 64 }, float text_size = 26.f);
        /*!
         *  \brief Sets up the menu textbox
         *  \param[in] position : the text position
         *  \param[in] color : the text color
         *  \param[in] text_size : The text size
         */
        void setupTextBox(const sf::Vector2f &position, const sf::Color &color = { 24, 160, 160 }, float text_size = 32.f);
        /*!
         *  \brief Adds player buttons to move in the grid
         */
        void addPlayerButtons();

    public :
        /*!
         *  \brief Prepares a menu accordingly to the current top screen
         *  \param[in] title : Menu title
         */
        void prepareMenu(const std::string &title);
        /*!
         *  \brief Prepares the campaign selector for highscores.
         */
        void prepareCampaignSelector();
        /*!
         *  \brief Prepares the menu for entering letting player type his name.
         *  \param[in] title : The title of the menu
         */
        void preparePlayerNamePrompt(const std::string &title);
        /*!
         *  \brief Prepares the scores to draw
         *  \param[in] title : The title of the menu
         *  \param[in] scores_list : The list of scores to intizialize (eg : 450, fifi33)
         */
        void prepareScores(const std::string &title, const std::vector<std::pair<int, std::string>> &scores_list = {});

        /*!
         *  \brief Player direction setter
         *  \param[in] new_direction : The new direction (corresponding to a model deltas index)
         */
        void setPlayerDirection(int new_direction);

        /*!
         *  \brief Loads a new theme.
         *  \param[in] theme_num : A string literal identifying the theme
         */
        void loadTheme(const std::string &theme_name);
        /*!
         *  \brief Allows to get each available themes
         *  \return The available themes
         */
        std::vector<std::string> getAvailableThemes() const;
        /*!
         *  \brief Provides available video modes as strings
         *  \return The available video modes strings
         */
        std::vector<std::string> getVideoModes() const;

    private :
        /*!
         *  \brief Sets the view accordingly to the window size
         */
        void initWindowView();

        /*!
         *  \brief Allows to get the scale factor of the grid
         *  \return The scale factor of the grid
         */
        float getGridScaleFactor() const;

        /*!
         *  \brief Draws the background
         */
        virtual void drawBackground();
        /*!
         *  \brief Draws the menu title
         */
        virtual void drawTitle();
        /*!
         *  \brief Draws player scores
         */
        virtual void drawScores();
        /*!
         *  \brief Draws a grid cell
         *  \param[in] cells_sprites : Cells sprites array
         *  \param[in] coords : Top-left corner of the cell once drawn
         *  \param[in] cell_str : String literal identifying the cell to draw
         *  \param[in] real_tile_size : Tile size on the window
         */
        virtual void drawCell(const std::vector<sf::Sprite> &cells_sprites, const sf::Vector2f &coords,
                              const std::string &cell_str, const sf::Vector2f &real_tile_size);
        /*!
         *  \brief Draws the grid
         *  \param[in] hidden : Cells content must be hidden
         */
        virtual void drawGrid(bool hidden = false);
        /*!
         *  \brief Draws the board used as a background for the highscores
         */
        virtual void drawHighscoresBoard();
        /*!
         *  \brief Draws a given score
         *  \param[in] score : The score to draw (eg : 1250 fifi33)
         *  \param[in] pos : Coordinates
         *  \param[in] score_height : The text height
         *  \param[in] rank : The rank
         */
        virtual void drawHighscore(const std::pair<int, std::string> &score, const sf::Vector2f &pos, float score_height,
                                   int rank);
        /*!
         *  \brief Draws the highscores stored in the list
         */
        virtual void drawHighscores();
        /*!
         *  \brief Draws every buttons in _menuButtons
         */
        virtual void drawGraphicButton(GraphicButton &graphic_button);
        /*!
         *  \brief Draws every selector in _menuSelectors
         */
        virtual void drawGraphicSelector(GraphicSelector &graphic_selector);
        /*!
         *  \brief Draws the _menuTextBox
         */
        virtual void drawMenuTextBox();
        /*!
         *  \brief Draws the menu (buttons, selectors and textbox)
         */
        virtual void drawMenu();
        /*!
         *  \brief Explains to the player that he can save his score
         */
        virtual void drawScoringExplanation();
    public :
        /*!
         *  \brief Draws everything. Entry point of the view.
         */
        virtual void draw() override;

        /*!
         *  \brief Prepares to animate the player movement
         *  \param[in] old_grid : The grid before the movement
         *  \param[in] moves_done : The number of cells crossed during the movement
         */
        void animatePlayerMove(const GameGrid &old_grid, int moves_done);
        /*!
         *  \brief Prepares to animate the player unmovement
         *  \param[in] old_grid : The grid before the unmovement
         *  \param[in] moves_done : The number of cells uncrossed during the unmovement
         */
        void animatePlayerUnmove(const GameGrid &old_grid, int moves_done);
        /*!
         *  \brief Prepares to animate the player explosion
         */
        void animatePlayerExplosion();
        /*!
         *  \brief Stops any playing animation
         */
        void stopAnimation();

    private :
        /*!
         *  \brief Sets the current music and stops the others
         *  \param[in] track : the track key to play. std::string() if you want to stop all music
         */
        void setCurrentMusicTrack(const std::string &track);
    public :
        /*!
         *  \brief Plays music if it is enabled, or stos it if it is disabled
         */
        void checkMusicState();
};


#endif
