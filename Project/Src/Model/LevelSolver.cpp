/*! \file LevelSolver.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include "LevelSolver.hpp"
#include "GameData.hpp"


LevelSolver::LevelSolver(GameData* level) : _level(level), _currentBranch(&_tree)
{}

LevelSolver::LevelSolver(const LevelSolver &level_solver)
        : _level(level_solver._level), _tree(level_solver._tree), _currentBranch(&_tree)
{}


LevelSolver &LevelSolver::operator=(const LevelSolver &level_solver)
{
    _level = level_solver._level;
    _tree = level_solver._tree;
    _currentBranch = &_tree;
    return *this;
}


Movement LevelSolver::getLastMovement() const
{
    return _currentBranch->move;
}


//////////////
// Obsolete //
//////////////
bool LevelSolver::isSolvable(const SolvingTree &solving_tree, int moves_done) const
{
    return false;
}

bool LevelSolver::isSolvable() const
{
    // Simple DFS...

    // If the player has won at this point, the DFS is finished, the level IS solvable.
    if (_level->getPlayer().getScores().getMoves() >= _level->getTarget())
        return true;

    int move;
    bool solvable;
    for (const auto &delta : GameData::deltas) // For each direction possible
    {
        move = _level->movePlayer(delta.first, delta.second, false); // We MUSN'T fill the tree.
        if (move) // If the move is successful
        {
            solvable = ((!_level->getPlayer().hasLost()) && (isSolvable())); // Then we have to check again.
            _level->unmovePlayer(delta.first, delta.second, move); // At the end, undo the move

            if (solvable)
                return true;
        }
    }

    return false;
}


void LevelSolver::fillTree(SolvingTree &solving_tree)
{
    int move;
    Movement movement;
    for (const auto &delta : GameData::deltas) // For each direction possible
    {
        move = _level->movePlayer(delta.first, delta.second, false);
        if (move) // If the move is possible, add it to the tree
        {
            movement = { delta.first, delta.second, move };
            solving_tree.next.insert({ movement, { movement, &solving_tree } });
            _level->unmovePlayer(delta.first, delta.second, move); // undo the move
        }
    }
}

void LevelSolver::fillTree()
{
    fillTree(_tree);
}


int LevelSolver::goToBranch(int dx, int dy)
{
    auto st_iter = _currentBranch->next.find({ dx, dy }); // Find the branch
    if (st_iter != _currentBranch->next.end()) // If the branch exists
    { // Go to this branch
        fillTree(st_iter->second);
        _currentBranch = &st_iter->second;
        return _currentBranch->move.movement; // return the amount of moves
    }
    return 0;
}

Movement LevelSolver::comeBackFromBranch()
{
    const Movement move = _currentBranch->move;
    if (_currentBranch->prev) // If there is a previous branch
    { // clear the current branch and update it
        _currentBranch->next.clear();
        _currentBranch = _currentBranch->prev;
    }
    return move;
}
