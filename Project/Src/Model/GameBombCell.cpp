/*! \file GameBombCell.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include "GameBombCell.hpp"


GameCell* GameBombCell::clone() const
{
    return new GameBombCell(*this);
}

std::string GameBombCell::toString(PlayerCellState player) const
{
    return (player != PlayerCellState::ELSEWHERE) ? "$@$" : (getCrossed()) ? "x@x" : "<@>";
}


void GameBombCell::applyCrossing(Player &player)
{
    player.lose();
}

void GameBombCell::undoCrossing(Player &player)
{
    player.resurrect();
}
