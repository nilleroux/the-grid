/*! \file SolvingTree.hpp
 * \brief The tree used in the level solver.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_SOLVINGTREE_HPP
#define PPD_SOLVINGTREE_HPP


#include <cstddef>

#include <map>

#include "Movement.hpp"


/*! \struct SolvingTree
* \brief This class is the tree used in the LevelSolver
*/
struct SolvingTree
{
    Movement move; ///< Movement done
    SolvingTree* prev; ///< Previous tree
    std::map<Movement, SolvingTree> next; ///< Next trees

    /*! 
     *  \brief SolvingTree constructor
     *  \param[in] move : the movement of this SolvingTree
     *  \param[in] prev : the previous SolvingTree
     */
    SolvingTree(const Movement &move = {}, SolvingTree* prev = NULL);
};


#endif
