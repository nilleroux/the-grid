/*! \file ConsoleController.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */


#include <vector>
#include <map>

#include "ConsoleController.hpp"


ConsoleController::ConsoleController(Model &model, ConsoleView &view) : Controller(model, view), _consoleView(&view)
{}


void ConsoleController::menu()
{
    int choice;

    // main menu's possible choices.
    _consoleView->prepareMenu("game_title", { "play", "play_campaign", "highscores", "editor", "options", "exit" });
    _view->draw();
    choice = _consoleView->askChoice();

    // screens which can possibly be called
    const std::vector<Screen> possible_screens =
            { Screen::GAME_INIT, Screen::GAME_SELECT_CAMPAIGN, Screen::HIGHSCORES, Screen::EDITOR, Screen::OPTIONS };

    if (choice == 0) // before playing, we must set the game up
        _model->getGameData().newGame(_model->getDifficulty()); // accordingly to selected options

    // if an option different from quitting was selected
    if ((0 <= choice) && (choice < static_cast<int>(possible_screens.size())))
        nextScreen(possible_screens[choice]); // push the new screen
    else
        exit();
}

void ConsoleController::mainOptions(int choice)
{
    // Screens which can possibly be called
    const std::vector<OptionsScreen> possible_screens =
            { OptionsScreen::LANGUAGE, OptionsScreen::DIFFICULTY, OptionsScreen::NONE };
   
    // If the choice is correct
    if ((0 <= choice) && (choice < static_cast<int>(possible_screens.size())))
    {
        // if “back” is selected, pop the current screen
        if (possible_screens[choice] == OptionsScreen::NONE)
        {
            _optionsScreens.pop();
            prevScreen();
        }
        else // else, push the new screen
            _optionsScreens.push(possible_screens[choice]);
    }
    else
        exit();
}

void ConsoleController::changeLanguage(int choice)
{
    // if a language was selected
    if ((0 <= choice) && (choice < static_cast<int>(_model->getLanguages().size())))
        _model->changeLanguage(_model->getLanguages()[choice].first); // change language to the selected one
    _optionsScreens.pop(); // pop the current screen
}

void ConsoleController::changeDifficulty(int choice)
{
    if ((0 <= choice) && (choice < static_cast<int>(Difficulty::LAST)))
        _model->changeDifficulty(static_cast<Difficulty>(choice));
    _optionsScreens.pop();
}

void ConsoleController::options()
{
    // Options screens data
    struct OptionsScreenProperties
    {
        std::string title; // Option screen's title
        std::vector<std::string> labels;  // options' labels
        void(ConsoleController::*fct)(int choice); // pointer to the function to call in case of choice 
        int selected_option; // previously selected option (marked by a star)
    };

    std::vector<std::string> languages_names;
    int selected_language = -1, lang_num = 0;
    // Adding each language to languages_names to set language menu's possible choices later
    for (const auto &language : _model->getLanguages())
    {
        languages_names.push_back(language.first);
        if (language.first == _model->getOption("language"))
            selected_language = lang_num;
        lang_num++;
    }
    languages_names.push_back("back"); // finally, the "back" choice

    const std::map<OptionsScreen, OptionsScreenProperties> options_properties =
            { { OptionsScreen::MAIN,
                { "options_title", { "change_language", "change_difficulty", "back", "exit" },
                  &ConsoleController::mainOptions, -1 } },
              { OptionsScreen::LANGUAGE,
                { "language_title", languages_names,
                  &ConsoleController::changeLanguage, selected_language } },
              { OptionsScreen::DIFFICULTY,
                { "difficulty_title", { "easy", "medium", "hard", "back" },
                   &ConsoleController::changeDifficulty, static_cast<int>(_model->getDifficulty()) } } };
    int choice;

    if (_optionsScreens.empty()) // if there is no option screen selected
        _optionsScreens.push(OptionsScreen::MAIN); // Show the main option screen

    // if the current option screen is one of our handled screens
    if (options_properties.find(_optionsScreens.top()) != options_properties.end())
    {
        // draw the menu
        _consoleView->prepareMenu(options_properties.at(_optionsScreens.top()).title,
                options_properties.at(_optionsScreens.top()).labels, options_properties.at(_optionsScreens.top()).selected_option);
        _view->draw();
        choice = _consoleView->askChoice();
        (this->*options_properties.at(_optionsScreens.top()).fct)(choice); // and call the right function
    }
    else // if the screen is unknown, then we should pop it.
        _optionsScreens.pop();
}


void ConsoleController::highscores()
{
    int choice;

    _consoleView->prepareMenu("highscores_title", { "easy", "medium", "hard", "campaign", "back" });
    _view->draw();
    choice = _consoleView->askChoice();

    // Screens which can possibly be called
    const std::vector<Difficulty> difficulties = { Difficulty::EASY, Difficulty::MEDIUM, Difficulty::HARD };

    // if a difficulty was selected
    if ((0 <= choice) && (choice < static_cast<int>(difficulties.size())))
    {
        std::vector<std::pair<int, std::string>> scores_list = GameData::loadScores(difficulties[choice]);
        if (scores_list.empty())
            _consoleView->displayMessage("no_score");
        else
            _consoleView->drawHighscores(scores_list); // showing scores for this difficulty
    }
    // otherwise, we can show campaign high scores
    else if (choice == static_cast<int>(difficulties.size()))
    {
        std::vector<std::pair<int, std::string>> scores_list = GameData::loadScores(_consoleView->askInteger("campaign_num"));
        if (scores_list.empty())
            _consoleView->displayMessage("no_score");
        else
            _consoleView->drawHighscores(scores_list);
    }
    else
        prevScreen();
}


void ConsoleController::gameInit()
{
    int choice;

    _consoleView->prepareMenu("start_game", { "start", "abandon", "exit" }); // And the starting menu
    _view->draw();
    choice = _consoleView->askChoice();

    // Let's create a vector which contains pointers to functions that may be called, ordered accordingly to the menu.
    const std::vector<void(ConsoleController::*)()> fcts =
            { &ConsoleController::startGame, &ConsoleController::prevScreen, &ConsoleController::exit };
    if ((0 <= choice) && (choice < static_cast<int>(fcts.size())))
        (this->*fcts[choice])();
}

void ConsoleController::gameSelectCampaign()
{
    int choice;
    std::vector<std::string> campaign_names = GameData::getCampaignNames();
    campaign_names.push_back("back");

    _consoleView->prepareMenu("campaign_title", campaign_names);
    _view->draw();
    choice = _consoleView->askChoice();

    prevScreen();

    if (choice < static_cast<int>(campaign_names.size()) - 1)
    {
        nextScreen(Screen::GAME_INIT);
        _model->getGameData().newGame(choice + 1);
        // a campaign without any level is rejected
        if (_model->getGameData().isCampaignFinished())
        {
            _consoleView->displayMessage("finished_title");
            prevScreen();
        }
    }
}

void ConsoleController::game()
{
    const std::vector<std::pair<int, int>> &deltas = GameData::deltas;
    const std::vector<void(ConsoleController::*)()> fcts =
            { &ConsoleController::undoLastMove, &ConsoleController::autodestruct, &ConsoleController::abandon,
              &ConsoleController::abandonAndExit };
    int choice;

    _consoleView->prepareMenu("playing_title", {}); // this is not a standard menu
    _view->draw();
    // There is as many choice possible as moves available + other available options
    choice = _consoleView->askChoice(deltas.size() + fcts.size());

    if (choice >= static_cast<int>(deltas.size())) // if user's choice is not a move
        (this->*fcts[choice - deltas.size()])(); // then call the appropriated function
    else
    {
        _model->getGameData().checkTimeLeft(); // check if the move is done in time

        // if the move failed and the move is not on the spot and the player has not lost, alert that the move is incorrect.
        if (((!deltas[choice].first) && (!deltas[choice].second)) || (_model->getGameData().getPlayer().hasLost()));
        else if (!_model->getGameData().movePlayer(deltas[choice].first, deltas[choice].second))
            _consoleView->displayMessage("incorrect_movement");

        if ((_model->getGameData().getPlayer().hasLost()) || (_model->getGameData().playerHasWon())) // if level is over
        {
            if (_model->getGameData().playerHasWon()) // if player has won
                _model->getGameData().addTimeLeftBonus(); // add bonus accordingly to time left
            prevScreen();
            nextScreen(Screen::LEVEL_END); // end the level
        }
    }
}

void ConsoleController::levelEnd()
{
    std::vector<void(ConsoleController::*)()> fcts =
            { &ConsoleController::nextLevel, &ConsoleController::abandon, &ConsoleController::abandonAndExit };
    int choice;

    // show the correct message and menu
    if (_model->getGameData().isCampaignFinished())
    {
        fcts = { &ConsoleController::abandon, &ConsoleController::abandonAndExit };
        _consoleView->prepareMenu("finished_title", { "finish", "exit" });
    }
    else if (!_model->getGameData().getPlayer().hasLost())
        _consoleView->prepareMenu("won_title", { "go_forward", "abandon", "exit" });
    else if (_model->getGameData().getPlayer().getScores().getLives() <= 0)
        _consoleView->prepareMenu("lost_title", { "restart", "abandon", "exit" });
    else
        _consoleView->prepareMenu("fail_title", { "retry", "abandon", "exit" });
    _view->draw();
    choice = _consoleView->askChoice();

    if ((0 <= choice) && (choice < static_cast<int>(fcts.size())))
        (this->*fcts[choice])();
}


void ConsoleController::editor()
{
    // NOT IMPLEMENTED YET
    prevScreen();
}


void ConsoleController::abandon()
{
    const auto &highscores_list = _model->getGameData().loadScores();
    if ((_model->getGameData().getPlayer().getScores().getTotalScore() > 0)
     && ((static_cast<int>(highscores_list.size()) < GameData::max_stored_scores)
      || (_model->getGameData().getPlayer().getScores().getTotalScore() > highscores_list.back().first)))
        _model->getGameData().endGame(_consoleView->askPlayerName("name_for_highscore"));
    else
        _model->getGameData().endGame(std::string());
    prevScreen();
}

void ConsoleController::abandonAndExit()
{
    const std::vector<std::pair<int, std::string>> highscores_list = _model->getGameData().loadScores();
    if ((_model->getGameData().getPlayer().getScores().getTotalScore() > 0)
     && ((static_cast<int>(highscores_list.size()) < GameData::max_stored_scores)
      || (_model->getGameData().getPlayer().getScores().getTotalScore() > highscores_list.back().first)))
        _model->getGameData().endGame(_consoleView->askPlayerName("name_for_highscore"));
    else
        _model->getGameData().endGame(std::string());
    exit();
}

void ConsoleController::autodestruct()
{
    _model->getGameData().autodestruct();
    prevScreen();
    nextScreen(Screen::LEVEL_END);
}

void ConsoleController::undoLastMove()
{
    if (!_model->getGameData().unmovePlayer()) // undo only if it's possible
        _consoleView->displayMessage("incorrect_unmovement");
    else
        _model->getGameData().loseTime(std::chrono::seconds(4)); // lose 4 seconds
}
