/*! \file GraphicView.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>

#include <SFML/Graphics.hpp>

#include "GraphicView.hpp"
#include "../Model/GameNormalCell.hpp"


const int GraphicView::tile_w = 48, GraphicView::tile_h = 48, GraphicView::buttons_width = 250, GraphicView::buttons_height = 100,
          GraphicView::explosion_frame_w = 64, GraphicView::explosion_frame_h = 64;
const sf::FloatRect GraphicView::grid_rect(275.f, 90.f, 500.f, 500.f);


sf::String GraphicView::getDrawableText(const std::string &utf8_text)
{
    std::basic_string<sf::Uint32> text;
    sf::Utf8::toUtf32(utf8_text.begin(), utf8_text.end(), std::back_inserter(text));
    return text;
}

float GraphicView::getTextHeight(const sf::Text &text)
{
    return text.getFont()->getLineSpacing(text.getCharacterSize());
}


GraphicView::GraphicView(const Model &model) : View(model)
{
    // Creating the main window
    sf::Image window_icon;
    if (window_icon.loadFromFile("Images/icon.png"))
        _renderWindow.setIcon(window_icon.getSize().x, window_icon.getSize().y, window_icon.getPixelsPtr());
    if (_model->getOption("fullscreen") == "y")
    {
        std::istringstream mode_buf(_model->getOption("resolution"));
        int w, h;
        char crt;
        mode_buf >> w >> crt >> crt >> h;
        createWindow(true, sf::VideoMode(w, h));
    }
    else
        createWindow();

    // Loading the theme
    const std::string theme_name = _model->getOption("theme");
    loadTheme((theme_name.empty()) ? getAvailableThemes().front() : theme_name);

    // Loading timeline colors
    _timelineColors.loadFromFile("Images/timeline.png");

    // List of musics to load
    const std::vector<std::pair<std::string, std::string>> musics = { { "menu", "menu.ogg" }, { "game", "game.ogg" } };
    for (const auto &music : musics)
    {
        _loadedMusics[music.first].openFromFile("Audio/Musics/" + music.second);
        _loadedMusics[music.first].setLoop(true);
    }

    _menuTextBox.active = false; // Setting the menu textbox to inactive
}

GraphicView::~GraphicView()
{
    _renderWindow.close();
}


void GraphicView::createWindow(bool fullscreen, const sf::VideoMode &video_mode)
{
    _renderWindow.create(video_mode, getDrawableText(_model->getLabel("game_title")),
            (fullscreen) ? sf::Style::Fullscreen : sf::Style::Default);
    initWindowView();
}


bool GraphicView::isAnimatingGame() const
{
    return ((_animationType) && ((_model->getTopScreen() == Screen::GAME) || (_model->getTopScreen() == Screen::LEVEL_END)));
}

int GraphicView::getAction()
{
    sf::Event event;
    while (_renderWindow.pollEvent(event))
        switch (event.type)
        {
            case sf::Event::KeyPressed :
                switch (event.key.code)
                {
                    case sf::Keyboard::Return : // Enter key
                        if (!_menuButtons.empty()) // If there is at least a menu button
                            return _menuButtons.front().id; // The id of the first one is returned
                        break;
                    case sf::Keyboard::Escape : // Escape key
                        if (!_menuButtons.empty()) // If there is at least a menu button
                            return _menuButtons.back().id; // The id of the last one is returned
                        return -1; // If no button, we should immediately exit the application
                    case sf::Keyboard::BackSpace : // Backspace key
                        if ((_menuTextBox.active) && (!_menuTextBox.typed_text.isEmpty()))
                        {   // Can clear textbox value if there is one
                            if (event.key.shift)
                                _menuTextBox.typed_text.clear();
                            else
                                _menuTextBox.typed_text.erase(_menuTextBox.typed_text.getSize() - 1);
                            _menuTextBox.cursor_clock.restart();
                        }
                        break;
                    default : // If another key is pressed and it looks like a numpad figure during game
                        if ((_model->getTopScreen() == Screen::GAME)
                         && (sf::Keyboard::Numpad1 <= event.key.code) && (event.key.code <= sf::Keyboard::Numpad9))
                        {        // We simulate the pression of the corresponding button if it exists
                            int id = event.key.code - sf::Keyboard::Numpad1 + 10;
                            for (GraphicButton &button : _menuButtons)
                                if (button.id == id)
                                    return id;
                        }
                }
                break;
            case sf::Event::TextEntered : // Text typed
                if ((_menuTextBox.active) && (event.text.unicode >= ' '))
                {
                    _menuTextBox.typed_text += event.text.unicode; // Added to textbox value if there is one
                    _menuTextBox.cursor_clock.restart();            // (and the character is printable)
                }
                break;
            case sf::Event::MouseButtonPressed : // Mouse click
            {   // We need mouse coordinates equivalents on the view
                const sf::Vector2f mouse_pos = _renderWindow.mapPixelToCoords({ event.mouseButton.x, event.mouseButton.y });
                for (GraphicSelector &selector : _menuSelectors)
                    if (selector.text.getGlobalBounds().contains(mouse_pos))
                    {   // Each pointed selector (usually one or less) gets its value changed
                        if (event.mouseButton.button == sf::Mouse::Left)    // Left click: next choice
                            selector.selected_option = (selector.selected_option + 1) % selector.options.size();
                        else if (event.mouseButton.button == sf::Mouse::Right)  // Right click: previous choice
                            selector.selected_option =
                                    (selector.selected_option + selector.options.size() - 1) % selector.options.size();
                    }
                if (event.mouseButton.button == sf::Mouse::Left) // A left click needs also looking at buttons…
                    for (GraphicButton &button : _menuButtons) // The ID of the first pointed button found is returned
                        if (button.default_sprite.getGlobalBounds().contains(mouse_pos)) // (if there is one)
                            return button.id;
                break;
            }
            case sf::Event::Resized : // Window resize
                initWindowView();
                break;
            case sf::Event::Closed : // Window closed
                return -1;
            default : ;
        }
    return 0;
}

int GraphicView::getSelectedChoice(int selector_num) const
{
    return _menuSelectors[selector_num - 1].selected_option;
}

std::string GraphicView::getTextBoxTypedText() const
{
    std::string utf8_text;
    sf::Utf32::toUtf8(_menuTextBox.typed_text.begin(), _menuTextBox.typed_text.end(), std::back_inserter(utf8_text));
    return utf8_text;
}


void GraphicView::addMenuButton(int id, const sf::Vector2f &position, const std::string &label, float text_size, int sprite_type)
{
    const sf::Texture &buttons_image = _loadedImages["buttons"];
    // Button default sprite
    sf::Sprite default_sprite(buttons_image,
            { buttons_width * sprite_type, 0, buttons_width, buttons_height }),
    // Button hover sprite
               hover_sprite(buttons_image,
            { buttons_width * sprite_type, buttons_height, buttons_width, buttons_height });
    default_sprite.setOrigin(sf::Vector2f(default_sprite.getLocalBounds().width, default_sprite.getLocalBounds().height) / 2.f);
    default_sprite.setPosition(position);
    hover_sprite.setOrigin(sf::Vector2f(hover_sprite.getLocalBounds().width, hover_sprite.getLocalBounds().height) / 2.f);
    hover_sprite.setPosition(position);
    GraphicButton button; // Create the button
    button.default_sprite = default_sprite;
    button.hover_sprite = hover_sprite;
    button.id = id;
    button.label = label;
    button.default_text.setFont(_loadedFonts["font"]);
    button.default_text.setCharacterSize(text_size);
    button.default_text.setPosition(position);
    button.default_text.setColor({ 0, 0, 0 });
    button.hover_text = button.default_text;
    button.hover_text.setColor({ 64, 64, 64 });
    _menuButtons.push_back(button);
}

void GraphicView::addMenuSelector(int selected_option, const std::vector<std::string> &options, const sf::Vector2f &position,
                                  bool options_are_labels, const std::string &description, const sf::Color &desc_color,
                                  const sf::Color &color, const sf::Color &hover_color, float text_size)
{
    GraphicSelector selector;
    selector.selected_option = selected_option;
    selector.options = options;
    selector.options_are_labels = options_are_labels;
    selector.description.setString(getDrawableText(_model->getLabel(description)));
    selector.description.setFont(_loadedFonts["font"]);
    selector.description.setCharacterSize(text_size);
    selector.description.setPosition({ position.x, position.y - text_size * .6f });
    selector.description.setOrigin(
            sf::Vector2f(selector.description.getLocalBounds().width, getTextHeight(selector.description)) / 2.f);
    selector.description.setColor(desc_color);
    selector.text.setFont(_loadedFonts["font"]);
    selector.text.setStyle(sf::Text::Bold);
    selector.text.setCharacterSize(text_size);
    selector.text.setPosition({ position.x, position.y + text_size * .6f });
    selector.text.setColor(color);
    selector.hover_text = selector.text;
    selector.hover_text.setColor(hover_color);
    _menuSelectors.push_back(selector);
}

void GraphicView::setupTextBox(const sf::Vector2f &position, const sf::Color &color, float text_size)
{
    _menuTextBox.active = true;
    _menuTextBox.typed_text.clear();
    _menuTextBox.text.setFont(_loadedFonts["font"]);
    _menuTextBox.text.setCharacterSize(text_size);
    _menuTextBox.text.setPosition(position);
    _menuTextBox.text.setColor(color);
    _menuTextBox.cursor_clock.restart();
}

void GraphicView::addPlayerButtons()
{
    const float scale_factor = getGridScaleFactor(); // Player button size is the same as a cell size
    const sf::Texture &buttons_image = _loadedImages["highlight"];
    sf::Sprite default_sprite(buttons_image, { 0, 0, tile_w, tile_h }),
               hover_sprite(buttons_image, { tile_w, 0, tile_w, tile_h });
    default_sprite.setOrigin(sf::Vector2f(default_sprite.getLocalBounds().width, default_sprite.getLocalBounds().height) / 2.f);
    default_sprite.setScale({ scale_factor, scale_factor });
    hover_sprite.setOrigin(sf::Vector2f(hover_sprite.getLocalBounds().width, hover_sprite.getLocalBounds().height) / 2.f);
    hover_sprite.setScale({ scale_factor, scale_factor });

    GraphicButton button;
    button.default_sprite = default_sprite;
    button.hover_sprite = hover_sprite;

    const Player &player = _model->getGameData().getPlayer();
    const GameGrid &game_grid = _model->getGameData().getGrid();
    const sf::Vector2f real_tile_size(tile_w * scale_factor, tile_h * scale_factor), // tile size on the screen
                       // Top-left corner position of the grid on the screen
                       grid_topleft(grid_rect.left + (grid_rect.width - real_tile_size.x * game_grid.getWidth()) / 2.f,
                                    grid_rect.top + (grid_rect.height - real_tile_size.y * game_grid.getHeight()) / 2.f),
                       // Player coords on the screen
                       player_pos(grid_topleft.x + player.getX() * real_tile_size.x + real_tile_size.x / 2.f,
                                  grid_topleft.y + player.getY() * real_tile_size.y + real_tile_size.y / 2.f);
    int x, y, delta_num = 0;
    for (const auto &delta : GameData::deltas) // For each direction available
    {
        x = player.getX() + delta.first;
        y = player.getY() + delta.second;
        if (game_grid.isOnGrid(x, y)) // if neighbour cell exists
        {
            const GameNormalCell* normal_cell = dynamic_cast<const GameNormalCell*>(game_grid(x, y));
            if ((normal_cell) && (!normal_cell->getCrossed())) // and is not crossed
            {
                button.id = 10 + delta_num; // Create the button
                button.default_sprite.setPosition({ player_pos.x + delta.first * real_tile_size.x,
                        player_pos.y + delta.second * real_tile_size.y });
                button.hover_sprite.setPosition({ player_pos.x + delta.first * real_tile_size.x,
                        player_pos.y + delta.second * real_tile_size.y });
                _menuButtons.push_back(button);
            }
        }
        delta_num++;
    }
}

void GraphicView::prepareMenu(const std::string &title)
{
    _menuTitle = title;
    _menuButtons.clear();
    _menuSelectors.clear();
    _menuTextBox.active = false;

    int id;
    switch (_model->getTopScreen())
    {
        case Screen::MENU : // Top screen is menu
            setCurrentMusicTrack("menu");
            id = 1; // Create the main menu
            for (const auto &button_properties : std::vector<std::pair<std::string, sf::Vector2f>>(
                    { { "play", { 200.f, 180.f } }, { "play_campaign", { 600.f, 180.f } }, { "highscores", { 400.f, 290.f } },
                      { "editor", { 400.f, 400.f } }, { "options", { 250.f, 520.f } }, { "exit", { 550.f, 520.f } } }))
            {
                addMenuButton(id, button_properties.second, button_properties.first);
                id++;
            }
            break;
        case Screen::OPTIONS : // Top screen is options
        {
            std::vector<std::string> languages_names; // Obtain languages and find current
            int selected_language = 0, lang_num = 0;
            for (const auto &language : _model->getLanguages())
            {
                languages_names.push_back(language.first);
                if (language.first == _model->getOption("language"))
                    selected_language = lang_num;
                lang_num++;
            }
            const std::vector<std::string> themes = getAvailableThemes(); // Obtain themes and find current
            int selected_theme = 0, theme_num = 0;
            for (const std::string &theme : themes)
            {
                if (theme == _model->getOption("theme"))
                    selected_theme = theme_num;
                theme_num++;
            }
            std::vector<std::string> video_modes; // Obtain resolutions and find current
            int selected_mode = 0, mode_num = 0;
            video_modes = getVideoModes();
            for (const std::string &mode_str : video_modes)
            {
                if (mode_str == _model->getOption("resolution"))
                    selected_mode = mode_num;
                mode_num++;
            }

            addMenuSelector(static_cast<int>(_model->getDifficulty()), { "easy", "medium", "hard" }, { 150.f, 150.f },
                    true, "difficulty"); // Add difficulties selector
            addMenuSelector(selected_language, languages_names, { 400.f, 150.f }, true, "language"); // Add languages selector
            addMenuSelector(selected_theme, themes, { 650.f, 150.f }, true, "theme"); // Add themes selector
            addMenuSelector((_model->getOption("fullscreen") == "y") ? 0 : 1, { "yes", "no" }, { 220.f, 240.f }, true,
                    "fullscreen"); // Add window mode selector
            addMenuSelector(selected_mode, video_modes, { 580.f, 240.f }, false, "resolution");
            addMenuSelector((_model->getOption("music_enabled") != "n") ? 0 : 1, { "yes", "no" }, { 400.f, 320.f }, true,
                    "music_enabled"); // Add music toggle selector
            id = 1; // And finally, “default” buttons
            for (const auto &button_properties : std::vector<std::pair<std::string, sf::Vector2f>>(
                    { { "apply", { 150.f, 430.f } }, { "reset", { 650.f, 430.f } }, { "done", { 200.f, 540.f } },
                      { "cancel", { 600.f, 540.f } } }))
            {
                addMenuButton(id, button_properties.second, button_properties.first);
                id++;
            }
            break;
        }
        case Screen::HIGHSCORES : // Top screen is highscores
            _highscoresList.clear();
            _highscoreScreen = -1; // Set the highscore screen to the default one
            id = 1; // And create buttons to access highscores for each mode
            for (const auto &button_properties : std::vector<std::pair<std::string, sf::Vector2f>>(
                    { { "easy", { 240.f, 180.f } }, { "medium", { 400.f, 280.f } }, { "hard", { 560.f, 180.f } },
                      { "campaign", { 200.f, 460.f } }, { "back", { 600.f, 460.f } } }))
            {
                addMenuButton(id, button_properties.second, button_properties.first);
                id++;
            }
            break;
        case Screen::GAME_SELECT_CAMPAIGN : // Top screen is campaign selection
            addMenuSelector(0, GameData::getCampaignNames(), { 400.f, 180.f }, true, "campaign"); // Add campaign selector
            addMenuButton(1, { 400.f, 360.f }, "play");
            addMenuButton(2, { 400.f, 500.f }, "back");
            break;
        case Screen::GAME_INIT : // Top screen is game initialisation
            _playerDirection = _animationType = 0;
            addMenuButton(1, { 135.f, 420.f }, "start");
            addMenuButton(2, { 135.f, 540.f }, "abandon");
            break;
        case Screen::GAME : // Top screen is Game
            setCurrentMusicTrack("game");
            if (!_animationType)
            {
                addMenuButton(1, { 135.f, 400.f }, "cancel_move", 22.f, 1);
                addMenuButton(2, { 135.f, 480.f }, "autodestruct", 22.f, 1);
                addPlayerButtons();
                if (!_model->getGameData().getLevelSolver().getLastMovement().movement)
                { // If no movement has been done yet, the cancel-move button text must be drawn in grey
                    _menuButtons.front().default_text.setColor({ 96, 96, 96 });
                    _menuButtons.front().hover_text.setColor({ 144, 144, 144 });
                }
                else if (!_model->getGameData().getLevelSolver().isSolvable())
                { // If the level can't be solved anymore, the cancel-move button text must be drawn in red
                    _menuButtons.front().default_text.setColor({ 160, 80, 48 });
                    _menuButtons.front().hover_text.setColor({ 192, 112, 64 });
                }
            }
            addMenuButton(50, { 135.f, 560.f }, "abandon", 22.f, 1);
            break;
        case Screen::LEVEL_END : // Top screen is the end of a level
            if (_model->getGameData().isCampaignFinished()) // Add the button to finish
                addMenuButton(2, { 135.f, 480.f }, "finish"); // a campaign at the end
            else
            {
                addMenuButton(1, { 135.f, 420.f }, (!_model->getGameData().getPlayer().hasLost()) ? "go_forward" :
                        (_model->getGameData().getPlayer().getScores().getLives() <= 0) ? "restart" : "retry");
                addMenuButton(2, { 135.f, 540.f }, "abandon"); // We can abandon (stop the game) from any level ending
            }
            break;
        default : ;
    }
}

void GraphicView::prepareCampaignSelector()
{
    _menuSelectors.clear(); // Clear the selectors
    addMenuSelector(0, GameData::getCampaignNames(), { 400.f, 140.f }, true, "campaign"); // Add the campaign selector
}

void GraphicView::preparePlayerNamePrompt(const std::string &title)
{
    _menuTitle = title;
    _menuButtons.clear();
    addMenuButton(1, { 220.f, 500.f }, "done");
    addMenuButton(2, { 580.f, 500.f }, "cancel");
    setupTextBox({ 400.f, 300.f });
}

void GraphicView::prepareScores(const std::string &title, const std::vector<std::pair<int, std::string>> &scores_list)
{
    _menuTitle = title;
    _menuButtons.clear();
    _highscoreScreen = (_menuSelectors.empty()) ? 0 : 1; // Random mode or campaign mode
    _highscoresList = scores_list; // load the scores
    addMenuButton(1, { 400.f, 520.f }, "back");
}


void GraphicView::setPlayerDirection(int new_direction)
{
    _playerDirection = new_direction;
}


void GraphicView::loadTheme(const std::string &theme_name)
{
    const std::vector<std::pair<std::string, std::string>> images = // List of images to load
            { { "background", "background.png" }, { "buttons", "buttons.png" }, { "highscores-board", "highscores-board.png" },
              { "grid-back", "grid-back.png" }, { "cells", "all-cells.png" }, { "highlight", "cell-highlight.png" },
              { "player", "player.png" }, { "explosion", "explosion.png" } };
    for (const auto &image : images) // Images are loaded from the theme directory
        _loadedImages[image.first].loadFromFile("Images/" + theme_name + '/' + image.second);

    const std::vector<std::pair<std::string, std::string>> fonts = { { "font", "font.ttf" } };
    for (const auto &font : fonts) // Fonts work similarly
        _loadedFonts[font.first].loadFromFile("Fonts/" + theme_name + '/' + font.second);
}

std::vector<std::string> GraphicView::getAvailableThemes() const
{
    std::vector<std::string> themes_list;
    std::ifstream themes_file("Options/Themes.cfg", std::ios::in | std::ios::binary);
    std::string theme_name;
    while (themes_file >> theme_name) // getting each theme in the theme file
        themes_list.push_back(theme_name);
    return themes_list;
}

std::vector<std::string> GraphicView::getVideoModes() const
{
    std::set<std::string> sorted_modes;
    for (const sf::VideoMode &mode : sf::VideoMode::getFullscreenModes())
        if ((mode.width >= 640) && (mode.height >= 480))
        {
            std::ostringstream mode_buf;
            mode_buf << mode.width << "×" << mode.height;
            sorted_modes.insert(mode_buf.str());
        }
    return std::vector<std::string>(sorted_modes.begin(), sorted_modes.end());
}


void GraphicView::initWindowView()
{
    // There is a virtual size (for the view) and a real size (for the window)
    const float vw = 800.f, vh = 600.f, rw = _renderWindow.getSize().x, rh = _renderWindow.getSize().y,
    // The view may need an offset, either horizontal or vertical, to preserve its aspect ratio in the window
                ox = (vw / vh > rw / rh) ? 0.f : rw * vh / rh - vw, oy = (vh / vw > rh / rw) ? 0.f : rh * vw / rw - vh;
    _windowView = sf::View({ -ox / 2.f, -oy / 2.f, vw + ox, vh + oy });
    _renderWindow.setView(_windowView);
}


float GraphicView::getGridScaleFactor() const
{
    // The grid is scaled to fit in the reserved rectangle; it can't overflow this space but doesn't need to fill it completely
    return std::min(grid_rect.width / tile_w / _model->getGameData().getGrid().getWidth(),
                    grid_rect.height / tile_h / _model->getGameData().getGrid().getHeight());
}


void GraphicView::drawBackground()
{
    _renderWindow.clear({ 32, 32, 32 });

    // Draw the background
    _renderWindow.draw(sf::Sprite(_loadedImages["background"]));
}

void GraphicView::drawTitle()
{
    sf::Text title_text(getDrawableText(_model->getLabel(_menuTitle)), _loadedFonts["font"], 48);
    title_text.setStyle(sf::Text::Bold);
    title_text.setColor({ 240, 240, 64 });
    title_text.setOrigin(sf::Vector2f(title_text.getLocalBounds().width, getTextHeight(title_text)) / 2.f);
    title_text.setPosition({ 400.f, 40.f });
    _renderWindow.draw(title_text);
}

void GraphicView::drawScores()
{
    const GameData &data = _model->getGameData();
    const std::string colon_label = _model->getLabel("colon");
    std::vector<std::pair<std::string, int>> values_tags =
            { { "total_score", data.getPlayer().getScores().getTotalScore() }, { "level", data.getLevelNum() },
              { "goal", data.getTarget() }, { "moves", data.getPlayer().getScores().getMoves() },
              { "level_score", data.getPlayer().getScores().getLevelScore() },
              { "lives", data.getPlayer().getScores().getLives() }, {},
              { "bonus_quantity", data.getPlayer().getScores().getNbBonus() },
              { "time_bonus", static_cast<int>(data.getPlayer().getScores().getTimeBonus().count()) / 1000 },
              { "score_bonus", data.getPlayer().getScores().getScoreBonus() },
              { "lives_bonus", data.getPlayer().getScores().getLivesBonus() }, {} };
    if (_model->getGameData().getPlayer().getScores().getTimeLeftBonus())
        values_tags.push_back({ "time_left_bonus", _model->getGameData().getPlayer().getScores().getTimeLeftBonus() });
    if (_model->getGameData().getPlayer().getScores().getCampaignBonus())
        values_tags.push_back({ "campaign_bonus", _model->getGameData().getPlayer().getScores().getCampaignBonus() });

    float y = 90.f;
    for (const auto &value_tag : values_tags) // For each "line" of score
    {
        if (!value_tag.first.empty())
        {
            sf::Text score_tag_text(getDrawableText(_model->getLabel(value_tag.first + "_short") + colon_label),
                    _loadedFonts["font"], 220 / values_tags.size()); // draw the score
            score_tag_text.setOrigin({ score_tag_text.getLocalBounds().width, 0.f });
            score_tag_text.setPosition({ 200.f, y });
            score_tag_text.setColor({ 224, 224, 224 });
            _renderWindow.draw(score_tag_text);

            std::ostringstream score_buf; // Cast the score value
            score_buf << value_tag.second;
            sf::Text score_value_text(getDrawableText(score_buf.str()), _loadedFonts["font"], 240 / values_tags.size());
            score_value_text.setStyle(sf::Text::Bold);
            score_value_text.setPosition({ 200.f, y });
            score_value_text.setColor({ 255, 96, 255 });
            _renderWindow.draw(score_value_text);
        }
        y += 240.f / values_tags.size();
    }

    if (_model->getTopScreen() == Screen::GAME) // If the top screen is the game, we should draw the timeline
    {
        const int total_time_width = 200, elapsed_time = data.getElapsedTime().count(), time_limit = data.getTimeLimit().count(),
                  filled_time_width = total_time_width - std::min(total_time_width,
                          total_time_width * std::max(0, elapsed_time) / time_limit);
        sf::RectangleShape outline_rect({ 200.f, 20.f }), timeline_rect(sf::Vector2f(filled_time_width, 20.f));
        outline_rect.setPosition({ 37.5, 330.f });
        outline_rect.setFillColor(sf::Color::Transparent);
        outline_rect.setOutlineColor({ 48, 48, 48, 192 });
        outline_rect.setOutlineThickness(5.f);
        timeline_rect.setPosition({ 37.5, 330.f });
        timeline_rect.setFillColor(_timelineColors.getPixel(std::min(_timelineColors.getSize().x - 1,
                filled_time_width * _timelineColors.getSize().x / total_time_width), 0));
        _renderWindow.draw(outline_rect);
        _renderWindow.draw(timeline_rect);
    }
}

void GraphicView::drawCell(const std::vector<sf::Sprite> &cells_sprites, const sf::Vector2f &coords, const std::string &cell_str,
                           const sf::Vector2f &real_tile_size)
{
    const std::unordered_map<std::string, int> sprites_indexes = // cells' strings and corresponding position in sprite
            { { "<@>", 4 }, { "x@x", 4 }, { "-0-", 2 }, { "-X-", 3 }, { " 0 ", 0 }, { "xXx", 1 } };

    std::string cell_pattern = cell_str;
    int figure = -1;
    if (('0' <= cell_pattern[1]) && (cell_pattern[1] <= '9'))
    { // If there is a figure in the cell literal, then cell is actually valued
        figure = cell_pattern[1] - '0'; // We can get the value
        cell_pattern[1] = '0';
    }
    sf::Sprite cell_sprite = (sprites_indexes.find(cell_pattern) == sprites_indexes.end()) ? cells_sprites.front() :
            cells_sprites[sprites_indexes.at(cell_pattern)]; // And set the sprite corresponding to the cell
    cell_sprite.setPosition(coords);
    _renderWindow.draw(cell_sprite);
    if (figure >= 0) // If cell is valued
    {   // Draw its value
        sf::Text figure_text(std::string(1, '0' + figure), _loadedFonts["font"], real_tile_size.y * 4 / 5);
        figure_text.setColor({ 0, 0, 0 });
        figure_text.setOrigin(sf::Vector2f(figure_text.getLocalBounds().width, getTextHeight(figure_text)) / 2.f);
        figure_text.setPosition({ coords.x + real_tile_size.x / 2.f, coords.y + real_tile_size.y / 2.f });
        _renderWindow.draw(figure_text);
    }
}

void GraphicView::drawGrid(bool hidden)
{
    const float scale_factor = getGridScaleFactor();
    sf::Sprite all_cells_sprite(_loadedImages["cells"]);
    all_cells_sprite.setScale({ scale_factor, scale_factor });
    std::vector<sf::Sprite> cells_sprites;

    int x, y;
    for (x = 0; x < static_cast<int>(_loadedImages["cells"].getSize().x); x += tile_w)   // for each cell
        for (y = 0; y < static_cast<int>(_loadedImages["cells"].getSize().y); y += tile_h)
        {
            cells_sprites.push_back(all_cells_sprite); // set the sprite
            cells_sprites.back().setTextureRect({ x, y, tile_w, tile_h }); // and sprite subrect
        }

    const sf::Vector2f real_tile_size(tile_w * scale_factor, tile_h * scale_factor);
    sf::Sprite grid_sprite(_loadedImages["grid-back"]);
    grid_sprite.setOrigin(sf::Vector2f(grid_sprite.getLocalBounds().width, grid_sprite.getLocalBounds().height) / 2.f);
    grid_sprite.setScale({ _model->getGameData().getGrid().getWidth() * real_tile_size.x / grid_rect.width,
            _model->getGameData().getGrid().getHeight() * real_tile_size.y / grid_rect.height });
    grid_sprite.setPosition({ grid_rect.left + grid_rect.width / 2.f, grid_rect.top + grid_rect.height / 2.f });
    _renderWindow.draw(grid_sprite);

    const sf::Vector2f grid_topleft(
            grid_rect.left + grid_rect.width / 2.f - _model->getGameData().getGrid().getWidth() * real_tile_size.x / 2.f,
            grid_rect.top + grid_rect.height / 2.f - _model->getGameData().getGrid().getHeight() * real_tile_size.y / 2.f);
    sf::Vector2f pos;
    pos.x = grid_topleft.x;
    for (x = 0; x < _model->getGameData().getGrid().getWidth(); x++)
    {
        pos.y = grid_topleft.y;
        for (y = 0; y < _model->getGameData().getGrid().getHeight(); y++)
        {
            if (hidden) // if cells are hidden
            {               // showing default sprite
                cells_sprites.front().setPosition(pos);
                _renderWindow.draw(cells_sprites.front());
            }
            else // otherwise, another method draws the cell
                drawCell(cells_sprites, pos, _model->getGameData().getGrid()(x, y)->toString(), real_tile_size);
            pos.y += real_tile_size.y;
        }
        pos.x += real_tile_size.x;
    }

    if ((!hidden) && ((!_model->getGameData().getPlayer().hasLost()) || (_animationType)))
    {   // If cells are not hidden and the player hasn's lost, we can eventually draw the player, that can be animated
        sf::Sprite player_sprite(_loadedImages["player"],
                { _playerDirection * tile_w, 0, tile_w, tile_h });
        const Player &player = _model->getGameData().getPlayer();
        player_sprite.setScale({ scale_factor, scale_factor });
        player_sprite.setPosition(
                { grid_topleft.x + player.getX() * real_tile_size.x, grid_topleft.y + player.getY() * real_tile_size.y });

        if ((_animationType == 1) || (_animationType == 2))
        {
            const float player_speed = (_animationType == 2) ? .06f : .1f;
            if (_animationClock.getElapsedTime().asSeconds() / player_speed >= _playerMoves)
            {
                _animationType = 0;
                prepareMenu(_menuTitle);
            }
            else
            {
                const std::pair<int, int> &delta = GameData::deltas[_playerDirection];
                const sf::Vector2i old_player_pos(_model->getGameData().getPlayer().getX() - delta.first * _playerMoves,
                        _model->getGameData().getPlayer().getY() - delta.second * _playerMoves);
                const int current_progress = static_cast<int>(_animationClock.getElapsedTime().asSeconds() / player_speed);
                int move_num;
                x = old_player_pos.x + delta.first * current_progress;
                y = old_player_pos.y + delta.second * current_progress;
                for (move_num = current_progress + 1; move_num <= _playerMoves; move_num++)
                {
                    x += delta.first;
                    y += delta.second;
                    drawCell(cells_sprites, { grid_topleft.x + x * real_tile_size.x, grid_topleft.y + y * real_tile_size.y },
                            _oldGrid(x, y)->toString(), real_tile_size);
                }
                player_sprite.setPosition({ grid_topleft.x + (old_player_pos.x +
                        delta.first * _animationClock.getElapsedTime().asSeconds() / player_speed) * real_tile_size.x,
                                            grid_topleft.y + (old_player_pos.y +
                        delta.second * _animationClock.getElapsedTime().asSeconds() / player_speed) * real_tile_size.y });
            }
        }
        _renderWindow.draw(player_sprite);
        if (_animationType == 3)
        {
            const float explosion_speed = .5f;
            const int explosion_times = 2;
            if (_animationClock.getElapsedTime().asSeconds() >= explosion_speed * explosion_times)
            {
                _animationType = 0;
                prepareMenu(_menuTitle);
            }
            else
            {
                const int explosion_frames = _loadedImages["explosion"].getSize().x / explosion_frame_w,
                          explosion_index = static_cast<int>(_animationClock.getElapsedTime().asSeconds()
                                          * explosion_frames / explosion_speed) % explosion_frames;
                sf::Sprite explosion_sprite(_loadedImages["explosion"],
                        { explosion_index * explosion_frame_w, 0, explosion_frame_w, explosion_frame_h });
                explosion_sprite.setOrigin(sf::Vector2f(explosion_sprite.getLocalBounds().width,
                        explosion_sprite.getLocalBounds().height) / 2.f);
                explosion_sprite.setScale({ scale_factor, scale_factor });
                explosion_sprite.setPosition({ grid_topleft.x + player.getX() * real_tile_size.x + real_tile_size.x / 2.f,
                        grid_topleft.y + player.getY() * real_tile_size.y + real_tile_size.y / 2.f });
                _renderWindow.draw(explosion_sprite);
            }
        }
    }
}

void GraphicView::drawHighscoresBoard()
{
    sf::Sprite board_sprite(_loadedImages["highscores-board"]);
    board_sprite.setOrigin(sf::Vector2f(board_sprite.getLocalBounds().width, board_sprite.getLocalBounds().height) / 2.f);
    board_sprite.setPosition({ 400.f, 300.f });
    _renderWindow.draw(board_sprite);
}

void GraphicView::drawHighscore(const std::pair<int, std::string> &score, const sf::Vector2f &pos, float score_height, int rank)
{
    // Draw the player name
    std::ostringstream player_name_buf; // Casting score
    player_name_buf << rank << ". " << score.second;
    sf::Text player_name_text(getDrawableText(player_name_buf.str()), _loadedFonts["font"], score_height * 3 / 4);
    if (score.first == _highscoresList.front().first)
        player_name_text.setStyle(sf::Text::Italic);
    player_name_text.setColor({ 32, 192, 192 });
    player_name_text.setPosition({ pos.x - 20.f, pos.y });
    player_name_text.setOrigin({ player_name_text.getLocalBounds().width, 0.f });
    _renderWindow.draw(player_name_text);

    // And then the score
    std::ostringstream score_buf; // Casting score
    score_buf << score.first;
    sf::Text score_text(getDrawableText(score_buf.str()), _loadedFonts["font"], score_height * 3 / 4);
    score_text.setStyle(sf::Text::Bold);
    score_text.setColor({ 255, 144, 32 });
    score_text.setPosition(pos);
    _renderWindow.draw(score_text);
}

void GraphicView::drawHighscores()
{
    drawHighscoresBoard();
    if (_highscoresList.empty()) // If there is no score
    {   // We must say that there is no score in this list
        sf::Text no_score_text(getDrawableText(_model->getLabel("no_score")), _loadedFonts["font"], 30);
        no_score_text.setStyle(sf::Text::Bold);
        no_score_text.setColor({ 64, 48, 224 });
        no_score_text.setOrigin(sf::Vector2f(no_score_text.getLocalBounds().width, getTextHeight(no_score_text)) / 2.f);
        no_score_text.setPosition({ 400.f, 300.f });
        _renderWindow.draw(no_score_text);
    }
    else
    {
        const float scores_height = 36.f;
        float y;
        int i;
        // if there is less than 5 scores to draw
        if (static_cast<int>(_highscoresList.size()) < 5)
        {   // One centered column of scores
            y = 300.f - _highscoresList.size() * scores_height / 2.f;
            for (i = 0; i < static_cast<int>(_highscoresList.size()); ++i)
            {
                drawHighscore(_highscoresList[i], { 440.f, y }, scores_height, i + 1);
                y += scores_height;
            }
        }
        else // if there is more
        {
            y = 300.f - (_highscoresList.size() / 2 + _highscoresList.size() % 2) * scores_height / 2.f;
            // The first column
            for (i = 0; i < static_cast<int>(_highscoresList.size() / 2 + _highscoresList.size() % 2); ++i)
            {
                drawHighscore(_highscoresList[i], { 280.f, y }, scores_height, i + 1);
                y += scores_height;
            }
            y = 300.f - (_highscoresList.size() / 2 + _highscoresList.size() % 2) * scores_height / 2.f;
            // The second column
            for (; i < static_cast<int>(_highscoresList.size()); ++i)
            {
                drawHighscore(_highscoresList[i], { 680.f, y }, scores_height, i + 1);
                y += scores_height;
            }
        }
    }
}

void GraphicView::drawGraphicButton(GraphicButton &graphic_button)
{
    bool mouse_on_button = (graphic_button.default_sprite.getGlobalBounds().contains(_renderWindow.mapPixelToCoords(
            sf::Mouse::getPosition(_renderWindow))));
    // draw default or hover sprite
    _renderWindow.draw(mouse_on_button ? graphic_button.hover_sprite : graphic_button.default_sprite);
    if (!graphic_button.label.empty())
    {
        sf::Text &text = mouse_on_button ? graphic_button.hover_text : graphic_button.default_text; // draw button text
        text.setString(getDrawableText(_model->getLabel(graphic_button.label)));
        text.setOrigin(sf::Vector2f(text.getLocalBounds().width, getTextHeight(text)) / 2.f);
        _renderWindow.draw(text);
    }
}

void GraphicView::drawGraphicSelector(GraphicSelector &graphic_selector)
{
    sf::Text *text = &graphic_selector.text;
    std::string label = graphic_selector.options[graphic_selector.selected_option];
    if (graphic_selector.options_are_labels)
        label = _model->getLabel(label);
    text->setString(getDrawableText(label));
    text->setOrigin(sf::Vector2f(text->getLocalBounds().width, getTextHeight(*text)) / 2.f);
    if (text->getGlobalBounds().contains(_renderWindow.mapPixelToCoords(sf::Mouse::getPosition(_renderWindow))))
    {
        text = &graphic_selector.hover_text;
        text->setString(getDrawableText(label));
        text->setOrigin(sf::Vector2f(text->getLocalBounds().width, getTextHeight(*text)) / 2.f);
    }
    _renderWindow.draw(*text);
    _renderWindow.draw(graphic_selector.description);
}

void GraphicView::drawMenuTextBox()
{
    _menuTextBox.text.setString(_menuTextBox.typed_text);
    _menuTextBox.text.setOrigin(sf::Vector2f(_menuTextBox.text.getLocalBounds().width, getTextHeight(_menuTextBox.text)) / 2.f);
    _renderWindow.draw(_menuTextBox.text);
    if (_menuTextBox.cursor_clock.getElapsedTime().asMilliseconds() % 1000 < 500)
    {
        sf::RectangleShape cursor_rect({ 4.f, getTextHeight(_menuTextBox.text) });
        cursor_rect.setPosition({ _menuTextBox.text.getGlobalBounds().left + _menuTextBox.text.getGlobalBounds().width + 3.f,
                _menuTextBox.text.getPosition().y - getTextHeight(_menuTextBox.text) / 2.f });
        cursor_rect.setFillColor({ 224, 224, 224 });
        _renderWindow.draw(cursor_rect);
    }
}

void GraphicView::drawMenu()
{
    for (GraphicButton &button : _menuButtons) // draw each menu button
        drawGraphicButton(button);
    for (GraphicSelector &selector : _menuSelectors) // draw each graphic selector
        drawGraphicSelector(selector);
    if (_menuTextBox.active) // Draw the menu textbox
        drawMenuTextBox();
}

void GraphicView::drawScoringExplanation()
{
    drawHighscoresBoard();
    std::ostringstream explanation_buf;
    explanation_buf << _model->getLabel("name_for_highscore_beginning")
                    << _model->getGameData().getPlayer().getScores().getTotalScore()
                    << _model->getLabel("name_for_highscore_ending");
    sf::Text explanation_text(getDrawableText(explanation_buf.str()), _loadedFonts["font"], 24);
    explanation_text.setStyle(sf::Text::Bold);
    explanation_text.setColor({ 64, 48, 224 });
    explanation_text.setOrigin(sf::Vector2f(explanation_text.getLocalBounds().width, getTextHeight(explanation_text)) / 2.f);
    explanation_text.setPosition({ 400.f, 140.f });
    _renderWindow.draw(explanation_text);
}

void GraphicView::draw()
{
    // Background is drawn first
    drawBackground();
    // Title is drawn
    drawTitle();
    // And the rest depends on the current screen, unless there is a textbox?
    if (!_menuTextBox.active)
        switch (_model->getTopScreen())
        {
            case Screen::GAME : // Top screen is game
                drawScores();
                drawGrid();
                break;
            case Screen::GAME_INIT : // Top screen is game initialisation
                // Before the game, a hidden grid is shown
                drawGrid(true);
                break;
            case Screen::LEVEL_END : // Top screen is end level screen
                // At the end of the level, scores and the grid are displayed
                drawScores();
                drawGrid();
                break;
            case Screen::HIGHSCORES : // Top screen is high scores
                if (_highscoreScreen >= 0) // If there is an highscore list to draw
                    drawHighscores(); // Then, we draw it (otherwise, buttons are specified by the controller)
                break;
            default : ;
        }
    // If the textbox is active, we only draw a text to tell the player what he must type
    else if ((_model->getTopScreen() == Screen::GAME) || (_model->getTopScreen() == Screen::LEVEL_END))
        drawScoringExplanation();
    // If there are buttons, selectors or another kind of textbox, there is a menu
    drawMenu();
    _renderWindow.display();
}


void GraphicView::animatePlayerMove(const GameGrid &old_grid, int moves_done)
{
    _animationType = 1;
    _oldGrid = old_grid;
    _playerMoves = moves_done;
    // Starting a movement animation…
    _animationClock.restart();
}

void GraphicView::animatePlayerUnmove(const GameGrid &old_grid, int moves_done)
{
    _animationType = 2;
    _oldGrid = old_grid;
    _playerMoves = moves_done;
    // Starting an unmovement animation…
    _animationClock.restart();
}

void GraphicView::animatePlayerExplosion()
{
    _animationType = 3;
    // Starting an exploding animation…
    _animationClock.restart();
}

void GraphicView::stopAnimation()
{
    _animationType = 0; // Stopping any animation
}


void GraphicView::setCurrentMusicTrack(const std::string &track)
{
    if ((_model->getOption("music_enabled") != "n") && (track != _currentTrack))
    {   // If music is enabled and track must change
        if (!_currentTrack.empty()) // Previous track is stopped
            _loadedMusics[_currentTrack].stop();
        if (!track.empty())
            _loadedMusics[track].play(); // New track is played
    }
    _currentTrack = track; // New current track is saved
}

void GraphicView::checkMusicState()
{
    if (!_currentTrack.empty())
    {   // If a track should be playing
        if (_model->getOption("music_enabled") != "n") // If music is enabled
            _loadedMusics[_currentTrack].play(); // Current track is played
        else                                           // Else, it is disabled
            _loadedMusics[_currentTrack].stop(); // Current track is stopped
    }
}
