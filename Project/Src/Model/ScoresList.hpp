/*! \file ScoresList.hpp
 * \brief Player's scores are managed here, including the bonuses
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_MODEL_SCORE_HPP
#define PPD_MODEL_SCORE_HPP


#include <chrono>


/*! \class ScoresList
* \brief This class descibes the Player scores
*/
class ScoresList
{
        static const int _cellValueFactor; ///< Normal cells points earning factor.

        int _lives; ///< Remaining lives
        int _totalScore; ///< Total Score
        int _moves; ///< Moves done
        int _levelScore; ///< Level's score
        int _nbBonus; ///< Amount of bonus collected
        std::chrono::milliseconds _timeBonus; ///< Time bonus collected
        int _scoreBonus; ///< Score bonus collected
        int _livesBonus; ///< Lives bonus collected
        int _timeLeftBonus; ///< Time left bonus collected
        int _campaignBonus; ///< Campaign ending bonus collected

    public :
        /*! 
         *  \brief ScoresList constructor
         *  \param[in] lives : The default amount of lives given to the player
         */
        ScoresList(int lives = 0);

        /*! 
         *  \brief Resets all the scores
         */
        void resetLevelScores();

        /*! 
         *  \brief Lives getter
         *  \return The amount of lives remaining.
         */
        int getLives() const;
        /*! 
         *  \brief Total score getter
         *  \return Total score
         */
        int getTotalScore() const;
        /*! 
         *  \brief Moves done getter
         *  \return The amount of moves done
         */
        int getMoves() const;
        /*! 
         *  \brief Level Score getter
         *  \return The level score
         */
        int getLevelScore() const;
        /*! 
         *  \brief Bonus amount getter
         *  \return The amount of bonus collected
         */
        int getNbBonus() const;
        /*! 
         *  \brief Score bonus getter
         *  \return The score bonus
         */
        int getScoreBonus() const;
        /*! 
         *  \brief Time bonus getter
         *  \return The time bonus
         */
        const std::chrono::milliseconds &getTimeBonus() const;
        /*! 
         *  \brief Lives bonus getter
         *  \return The amount of lives bonus collected
         */
        int getLivesBonus() const;
        /*! 
         *  \brief Time left bonus getter
         *  \return The time left bonus
         */
        int getTimeLeftBonus() const;
        /*! 
         *  \brief Campaign Bonus getter
         *  \return The campaign bonus
         */
        int getCampaignBonus() const;

        /*! 
         *  \brief Loses a life
         */
        void loseLife();
        /*! 
         *  \brief Increases moves
         */
        void incrementMoves();
        /*! 
         *  \brief Increases cell score
         *  \param[in] value : the value to add to the score
         */
        void incrementCellScore(int value);
        /*! 
         *  \brief Applies a score bonus
         *  \param[in] value : the value of the bonus
         */
        void pickScoreBonus(int value);
        /*! 
         *  \brief Applies a time bonus
         *  \param[in] value : the value of the bonus
         */
        void pickTimeBonus(const std::chrono::milliseconds &value);
        /*! 
         *  \brief Applies a life bonus
         */
        void pickLifeBonus();
        /*! 
         *  \brief Adds a time left bonus
         *  \param[in] time_left : the time left bonus
         */
        void addTimeLeftBonus(const std::chrono::milliseconds &time_left);
        /*! 
         *  \brief Adds a campaign bonus
         *  \param[in] value : the bonus value
         */
        void addCampaignBonus(int value);

        /*! 
         *  \brief Adds a life
         */
        void recoverLife();
        /*! 
         *  \brief Subtract the score gathered from the undone move.
         */
        void undoMove();
        /*! 
         *  \brief Undoes the score of a cell
         *  \param[in] value : the value of the cell
         */
        void undoCellScore(int value);
        /*! 
         *  \brief Undoes the bonus gathered with a score bonus cell
         *  \param[in] value : the amount of points to subtract
         */
        void undoScoreBonus(int value);
        /*! 
         *  \brief Undoes the time gathered with a time bonus cell
         *  \param[in] value : the time to subtract
         */
        void undoTimeBonus(const std::chrono::milliseconds &value);
        /*! 
         *  \brief Undoes a life bonus
         */
        void undoLifeBonus();
};


#endif
