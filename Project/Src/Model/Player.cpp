/*! \file Player.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include "Player.hpp"


Player::Player() : _posX(0), _posY(0), _lost(false)
{}


int Player::getX() const
{
    return _posX;
}

int Player::getY() const
{
    return _posY;
}

const ScoresList &Player::getScores() const
{
    return _scores;
}

bool Player::hasLost() const
{
    return _lost;
}


void Player::setPos(int x, int y)
{
    _posX = x;
    _posY = y;
}

void Player::move(int dx, int dy)
{
    _posX += dx;
    _posY += dy;
}

ScoresList &Player::getScores()
{
    return _scores;
}

void Player::lose()
{
    _lost = true;
}

void Player::resurrect()
{
    _lost = false;
}
