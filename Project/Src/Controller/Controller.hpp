/*! \file Controller.hpp
 * \brief Abstract class for the controller in the game.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_CONTROLLER_HPP
#define PPD_CONTROLLER_HPP


#include "../Model/Model.hpp"
#include "../View/View.hpp"


/*! \class Controller
* \brief Abstract class for the game controller.
*/
class Controller
{
    protected :
        Model* _model; ///< The model associated with this Controller.
        View* _view; ///< The view associated with this Controller.

    public :
        /*!
         *  \brief Constructor of Controller class
         *
         *  \param[in] model : the model to associate to this Controller
         *  \param[in] view : the view to associate with this Controller.
         */
        Controller(Model &model, View &view);
        /*!
          *  \brief Destructor for a Controller.
          */
        virtual ~Controller() = default;

         /*!
          *  \brief Controller's entry point. "Do your things"
          */
        void act();

    private :
        /*!
         *  \brief Handles the main menu.
         */
        virtual void menu() = 0;
        /*!
         *  \brief Handles the options menu screens.
         */
        virtual void options() = 0;

        /*!
         *  \brief Handles the highscores screen.
         */
        virtual void highscores() = 0;

        /*!
         *  \brief Campaign selection screen.
         */
        virtual void gameInit() = 0;
        /*!
         *  \brief Handles the start of a new campaign.
         */
        virtual void gameSelectCampaign() = 0;
        /*!
         *  \brief Handles the game session.
         */
        virtual void game() = 0;
        /*!
         *  \brief Handles the end of a level.
         */
        virtual void levelEnd() = 0;

        /*!
         *  \brief Handles the level editor.
         *  \todo Do the level editor.
         */
        virtual void editor() = 0;

    protected :
         /*!
         *  \brief Allows to push a new screen on the stack (so that it becomes current)
         *  \param[in] screen : the screen to push.
         */
        void nextScreen(Screen screen);
        /*!
         *  \brief Pops the previous screen and the one below becomes current.
         */
        void prevScreen();
        /*!
         *  \brief Start to play the game
         */
        void startGame();
        /*!
         *  \brief Set next level.
         */
        void nextLevel();
        /*!
         *  \brief Exits the program (by popping out each screen).
         */
        void exit();
};


#endif
