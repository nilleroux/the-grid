/*! \file Movement.hpp
 *  \brief A class to represent a movement in the level solver tree.
 *  \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_MOVEMENT_HPP
#define PPD_MOVEMENT_HPP


/**
 * \struct Movement
 * \brief A class to represent a movement in the level solver tree.
*/
struct Movement
{
    int dx, ///< The X direction (-1, 0 or 1)
        dy, ///< The Y direction (-1, 0 or 1)
        movement; ///< The amount of moves in this direction

    /*! 
     *  \brief Movement constructor
     *  \param[in] dx : The X direction (-1, 0 or 1)
     *  \param[in] dy : The Y direction (-1, 0 or 1)
     *  \param[in] movement : The amount of moves in this direction
     */
    Movement(int dx = 0, int dy = 0, int movement = 0);

    /*! 
     *  \brief Inferiority operator for Movement
     *  \param[in] movement : the movement to compare
     *  \return true : current move is lower than movement in lexical order. false : otherwise
     */
    bool operator<(const Movement &movement) const;
};


#endif
