/*! \file GraphicElement.hpp
 *  \brief GraphicView graphic elements
 *  \author { Sylvain Chiron, Nils Le Roux }
 */
#ifndef PPD_BUTTON_HPP
#define PPD_BUTTON_HPP


#include <SFML/Graphics.hpp>


/*! \struct GraphicButton
* \brief A simple button
*/
struct GraphicButton
{
    int id;             ///< The id of the button
    std::string label;  ///< The button label key

    sf::Sprite default_sprite, ///< Default sprite drawn
               hover_sprite;   ///< Sprite drawn when mouse is over the button
    sf::Text default_text,     ///< Default text
             hover_text;       ///< Text drawn when the mouse is over the button
};


/*! \struct GraphicSelector
* \brief A kind of ComboBox. It lets you choose between a bunch of defined values.
*/
struct GraphicSelector
{
    int selected_option;                ///< The currently selected option
    std::vector<std::string> options;   ///< Vector of each options available
    bool options_are_labels;            ///< Tells whether to use or not the language file when displaying options

    sf::Text description, ///< Description text
             text,        ///< Text drawn
             hover_text;  ///< Text drawn when the mouse is over the selector value
};


/*! \struct GraphicTextBox
* \brief A TextBox. It lets you enter a value
*/
struct GraphicTextBox
{
    bool active; ///< The textbox is active
    sf::String typed_text; ///< The current value (content) of the textbox
    sf::Text text; ///< Textbox label
    sf::Clock cursor_clock; ///< Cursor clock
};


#endif
