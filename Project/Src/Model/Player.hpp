/*! \file Player.hpp
 * \brief The player has a position on the grid and a bunch of scores.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_MODEL_PLAYER_HPP
#define PPD_MODEL_PLAYER_HPP


#include "ScoresList.hpp"


/*! \class Player
* \brief This class descibes the Player
*/
class Player
{
        int _posX, ///< X-coordinate of the player
            _posY; ///< Y-coordinate of the player
        ScoresList _scores; ///< Scores of the player
        bool _lost; ///< The player has lost

    public :
        /*! 
         *  \brief Player default constructor
         */
        Player();

        /*! 
         *  \brief Player X-coordinate getter
         *  \return The X-coordinate of this player.
         */
        int getX() const;
        /*! 
         *  \brief Player Y-coordinate getter
         *  \return The Y-coordinate of this player.
         */
        int getY() const;
        /*! 
         *  \brief Player scores getter
         *  \return The scores list
         */
        const ScoresList &getScores() const;
        /*! 
         *  \brief Check whether the player has lost.
         *  \return true : the player has lost, false : otherwise
         */
        bool hasLost() const;

        /*! 
         *  \brief Set the player's position to the specified coordinates
         *  \param[in] x : x-coordinate
         *  \param[in] y : y-coordinate
         */
        void setPos(int x, int y);
        /**
         ** 
         ** dx x direction
         ** dy y direction
         **/
        /*! 
         *  \brief Moves the player in the specified direction
         *  \param[in] dx : X direction (-1, 0 or 1)
         *  \param[in] dy : Y direction (-1, 0 or 1)
         */
        void move(int dx, int dy);
        /*! 
         *  \brief Player scores getter
         *  \return The scores list
         */
        ScoresList &getScores();
        /*! 
         *  \brief Makes the player lose a life.
         */
        void lose();
        /*! 
         *  \brief Bring the player back to life. (Sets lose to false)
         */
        void resurrect();
};


#endif
