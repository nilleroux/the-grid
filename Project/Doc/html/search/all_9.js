var searchData=
[
  ['id',['id',['../struct_graphic_button.html#ab0d88683c53ab27564c76bac384ad318',1,'GraphicButton']]],
  ['incrementcellscore',['incrementCellScore',['../class_scores_list.html#ac4d8ee3492fb945c4edca87c7cb6f196',1,'ScoresList']]],
  ['incrementmoves',['incrementMoves',['../class_scores_list.html#a313c9016847f8908968030e088526cd8',1,'ScoresList']]],
  ['iscampaignfinished',['isCampaignFinished',['../class_game_data.html#a4e4f8c7ae39399110b9f481bd9293e8d',1,'GameData']]],
  ['isongrid',['isOnGrid',['../class_game_grid.html#aeaa91f04ea2137809ecfe7f9e1ab4625',1,'GameGrid']]],
  ['issolvable',['isSolvable',['../class_level_solver.html#a6bfcad40a12ea59f890ba83a65d9a5e1',1,'LevelSolver']]]
];
