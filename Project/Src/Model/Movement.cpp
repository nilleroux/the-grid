/*! \file Movement.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include "Movement.hpp"


Movement::Movement(int dx, int dy, int movement) : dx(dx), dy(dy), movement(movement)
{}


bool Movement::operator<(const Movement &movement) const
{
    return (dx == movement.dx) ? (dy < movement.dy) : (dx < movement.dx);
}
