/*! \file GameBombCell.hpp
 * \brief For grid bombs.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_MODEL_GAMEBOMBCELL_HPP
#define PPD_MODEL_GAMEBOMBCELL_HPP


#include "GameCell.hpp"

/*! \class GameBombCell
* \brief GameBombCell class defines the bomb cells in the grid.
*/
class GameBombCell : public GameCell
{
    public :
        /*!
         *  \brief Allows a Cell to be cloned
         */
        virtual GameCell* clone() const override;
        /*!
         *  \brief toString method for GameBombCell.
         *  \param[in] player : the cell state of the player (to draw it correctly)
         *  \return A string representation of the cell.
         */
        virtual std::string toString(PlayerCellState player = PlayerCellState::ELSEWHERE) const override;

    protected :
        /*!
         *  \brief Applies a crossing to a player (updates his state and scores)
         *  \param[in] player : the player on which crossing must be applied
         */
        virtual void applyCrossing(Player &player) override;
        /*!
         *  \brief Uncrosses the cell and update player state and scores.
         *  \param[in] player : the player on which crossing must be applied
         */
        virtual void undoCrossing(Player &player) override;
};


#endif
