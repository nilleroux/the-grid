/*! \file GameNormalCell.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include <sstream>
#include <iomanip>

#include "GameCell.hpp"
#include "GameNormalCell.hpp"


GameNormalCell::GameNormalCell(int value) : _value(value)
{}


GameCell* GameNormalCell::clone() const
{
    return new GameNormalCell(*this);
}


std::string GameNormalCell::toString(PlayerCellState player) const
{
    if (player == PlayerCellState::ONCELL)
        return "$$$";
    else if (player == PlayerCellState::DEADONCELL)
        return "$X$";
    else if (getCrossed())
        return "xXx";
    std::ostringstream buf;
    buf << std::setw(2) << getValue() << ' ';
    return buf.str();
}


void GameNormalCell::applyCrossing(Player &player)
{
    player.getScores().incrementCellScore(getValue());
}

void GameNormalCell::undoCrossing(Player &player)
{
    player.getScores().undoCellScore(getValue());
}


int GameNormalCell::getValue() const
{
    return _value;
}
