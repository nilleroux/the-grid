/*! \file ConsoleController.hpp
 * \brief The interface between the console view and the model.
 * \author { Sylvain Chiron, Nils Le Roux }
 */
#ifndef PPD_CONSOLECONTROLLER_HPP
#define PPD_CONSOLECONTROLLER_HPP


#include <stack>

#include "Controller.hpp"
#include "../View/ConsoleView.hpp"


/// Available options screens
enum class OptionsScreen
{
    NONE,       ///< No screen
    MAIN,       ///< Main option screen
    LANGUAGE,   ///< Language selection screen
    DIFFICULTY  ///< Difficulty selection screen
};


/*! \class ConsoleController
* \brief Class representing the interface between the ConsoleView and the Model.
*/
class ConsoleController : public Controller
{
    private :
        std::stack<OptionsScreen> _optionsScreens; ///< Stack of options screens. The top option screen is shown.

    protected :
        ConsoleView* _consoleView; ///< The view associated to this console controller

    public :
        /*!
         *  \brief Constructor of ConsoleController class
         *
         *  \param[in] model : the model to associate to this Controller
         *  \param[in] view : the view to associate with this Controller.
         */
        ConsoleController(Model &model, ConsoleView &view);

    private :
        /*!
         *  \brief Handles the main menu.
         */
        virtual void menu() override;
        /*!
         *  \brief Treats user choice
         *
         *  \param[in] choice : user's chosen value
         */
        virtual void mainOptions(int choice);
        /*!
         *  \brief Change the language accordingly to the user choice
         *
         *  \param[in] choice : chosen index value in the list.
         */
        virtual void changeLanguage(int choice);
        /*!
         *  \brief Sets a new difficulty level.
         *
         *  \param[in] choice : chosen index value in the list.
         */
        virtual void changeDifficulty(int choice);

        /*!
         *  \brief Handles the options menu screens.
         */
        virtual void options() override;

        /*!
         *  \brief Handles the highscores screen.
         */
        virtual void highscores() override;

        /*!
         *  \brief Campaign selection screen.
         */
        virtual void gameInit() override;
        /*!
         *  \brief Handles the start of a new campaign.
         */
        virtual void gameSelectCampaign() override;
        /*!
         *  \brief Game screen.
         */
        virtual void game() override;
        /*!
         *  \brief Handles the end of a level.
         */
        virtual void levelEnd() override;

        /*!
         *  \brief Handles the level editor.
         *  \todo Do the level editor.
         */
        virtual void editor() override;

    protected :
        /*!
         *  \brief Asks player's name (for highscores) and goes back to previous screen.
         */
        virtual void abandon();
        /*!
         *  \brief Asks player's name (for highscores) and exits the game.
         */
        virtual void abandonAndExit();
         /*!
         *  \brief Handles the undo move feature.
         */
        virtual void undoLastMove();
         /*!
         *  \brief Handles the autodestruct feature.
         */
        virtual void autodestruct();
};


#endif
