/*! \file GameCell.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include "GameCell.hpp"


GameCell::GameCell() : _crossed(false), _crossedTwice(false)
{}


bool GameCell::getCrossed() const
{
    return _crossed;
}

void GameCell::cross(Player &player)
{
    player.getScores().incrementMoves();
    if (_crossed)
    {
        player.lose();
        _crossedTwice = true;
    }
    else
    {
        applyCrossing(player);
        _crossed = true;
    }
}

void GameCell::uncross(Player &player)
{
    player.getScores().undoMove();
    if (_crossedTwice)
    {
        _crossedTwice = false;
        player.resurrect();
    }
    else
    {
        _crossed = false;
        undoCrossing(player);
    }
}
