/*! \file GameBonusCell.hpp
 * \brief For all bonuses in the grid.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_MODEL_GAMEBONUSCELL_HPP
#define PPD_MODEL_GAMEBONUSCELL_HPP


#include <chrono>

#include "GameNormalCell.hpp"
#include "ScoresList.hpp"

/*! \class GameBonusCell
* \brief GameBonusCell is an abstract class describing Bonus cells in the grid.
*/
class GameBonusCell : public GameNormalCell
{
    public :
        /*!
         *  \brief BonusCell constructor
         *  \param[in] value : the cell value
         */
        GameBonusCell(int value = 0);

        /*!
         *  \brief Allows a Cell to be cloned
         *  \return a pointer to the cloned cell.
         */
        virtual GameCell* clone() const override = 0;

        /*!
         *  \brief toString method for GameBonusCell.
         *  \param[in] player : the cell state of the player (to draw it correctly)
         *  \return A string representation of the cell.
         */
        virtual std::string toString(PlayerCellState player = PlayerCellState::ELSEWHERE) const override;

    protected :
        /*!
         *  \brief Applies a crossing to a player (updates his state and scores)
         *  \param[in] player : the player on which crossing must be applied
         */
        virtual void applyCrossing(Player &player) override;
        /*!
         *  \brief Uncrosses the cell and update player state and scores.
         *  \param[in] player : the player on which crossing must be applied
         */
        virtual void undoCrossing(Player &player) override;

        /*!
         *  \brief Apply a bonus to a ScoresList.
         *  \param[in] scores : The ScoresList
         */
        virtual void applyBonus(ScoresList &scores) = 0;
        /*!
         *  \brief Removes a bonus from a ScoresList.
         *  \param[in] scores The ScoresList
         **/
        virtual void undoBonus(ScoresList &scores) = 0;
};

/*! \class GameTimeBonusCell
* \brief This class describes time bonus cells in the grid.
*/
class GameTimeBonusCell : public GameBonusCell
{
        std::chrono::milliseconds _timeBonus; ///< Bonus time allocated to the cell

    public :
        /*!
         *  \brief GameTimeBonusCell constructor
         *  \param[in] value : the cell value
         *  \param[in] time_bonus : the bonus allocated to the cell
         */
        GameTimeBonusCell(int value = 0, const std::chrono::milliseconds &time_bonus = std::chrono::seconds(0));

        /*!
         *  \brief Allows a Cell to be cloned
         *  \return a pointer to the cloned cell.
         */
        virtual GameCell* clone() const override;

    protected :
        /*!
         *  \brief Apply a bonus to a ScoresList.
         *  \param[in] scores : The ScoresList
         */
        virtual void applyBonus(ScoresList &scores) override;
        /*!
         *  \brief Removes a bonus from a ScoresList.
         *  \param[in] scores The ScoresList
         **/
        virtual void undoBonus(ScoresList &scores) override;
};

/*! \class GameScoreBonusCell
* \brief This class describes score bonus cells in the grid.
*/
class GameScoreBonusCell : public GameBonusCell
{
        int _scoreBonus; ///< The bonus score allocated to the cell

    public :
        /*!
         *  \brief GameScoreBonusCell constructor
         *  \param[in] value : the cell value
         *  \param[in] score_bonus : the bonus allocated to the cell
         */
        GameScoreBonusCell(int value = 0, int score_bonus = 0);

        /*!
         *  \brief Allows a Cell to be cloned
         *  \return a pointer to the cloned cell.
         */
        virtual GameCell* clone() const override;

    protected :
        /*!
         *  \brief Apply a bonus to a ScoresList.
         *  \param[in] scores : The ScoresList
         */
        virtual void applyBonus(ScoresList &scores) override;
        /*!
         *  \brief Removes a bonus from a ScoresList.
         *  \param[in] scores The ScoresList
         **/
        virtual void undoBonus(ScoresList &scores) override;
};

/*! \class GameLifeBonusCell
* \brief This class describes life bonus cells in the grid.
* One life is won when a GameLifeBonusCell is crossed.
*/
class GameLifeBonusCell : public GameBonusCell
{
    public :
        /*!
         *  \brief GameLifeBonusCell constructor
         *  \param[in] value : the cell value
         */
        GameLifeBonusCell(int value = 0);

        /*!
         *  \brief Allows a Cell to be cloned
         *  \return a pointer to the cloned cell.
         */
        virtual GameCell* clone() const override;

    protected :
        /*!
         *  \brief Apply a bonus to a ScoresList.
         *  \param[in] scores : The ScoresList
         */
        virtual void applyBonus(ScoresList &scores) override;
        /*!
         *  \brief Removes a bonus from a ScoresList.
         *  \param[in] scores The ScoresList
         **/
        virtual void undoBonus(ScoresList &scores) override;
};


#endif
