/*! \file GameCell.hpp
 * \brief Because GameGrid is composed of cells.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_MODEL_GAMECELL_HPP
#define PPD_MODEL_GAMECELL_HPP


#include <string>

#include "Player.hpp"


/// Available states for a player on a cell.
enum class PlayerCellState
{
    ELSEWHERE,  ///< The player is not on the cell
    ONCELL,     ///< The player is over the cell
    DEADONCELL  ///< The player is dead on the cell :(
};


/*! \class GameCell
* \brief GameCell is an abstract class describing the cells in the grid.
*/
class GameCell
{
        bool _crossed, ///< the cell has been crossed by the player
             _crossedTwice; ///< the cell has been crossed twice by the player (the player has lost)

    public :
        /*!
         *  \brief GameCell default constructor
         */
        GameCell();
        /*!
         *  \brief GameCell destructor
         */
        virtual ~GameCell() = default;

        /*!
         *  \brief Allows a Cell to be cloned. Useful for the GameGrid copy constructor.
         *  \return a pointer to the cloned cell.
         */
        virtual GameCell* clone() const = 0;

         /*!
         *  \brief Converts a cell to a string.
         *  \param[in] player : the cell state of the player (to draw it correctly)
         *  \return A string representation of the cell.
         */
        virtual std::string toString(PlayerCellState player = PlayerCellState::ELSEWHERE) const = 0;

        /*!
         *  \brief Check whether the cell is crossed.
         *  \return return the state of the cell (crossed or not)
         */
        bool getCrossed() const;
        /*!
         *  \brief Applies a crossing to a player (updates his state and scores)
         *  \param[in] player : the player on which crossing must be applied
         */
        void cross(Player &player);
         /*!
         *  \brief Undo crossing.
         *  \param[in] player : the player on which uncrossing must be applied
         */
        void uncross(Player &player);
    protected :
         /*!
         *  \brief Apply crossing to the player.
         *  \param[in] player : The player.
         */
        virtual void applyCrossing(Player &player) = 0;
         /*!
         *  \brief Undo crossing to the cell and the player.
         *  \param[in] player : The player.
         */
        virtual void undoCrossing(Player &player) = 0;
};


#endif
