/*! \file View.hpp
 * \brief Abstract class for the view in the game.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#ifndef PPD_VIEW_HPP
#define PPD_VIEW_HPP


#include "../Model/Model.hpp"


/*! \class View
* \brief Abstract class for the view in the game.
*/
class View
{
	protected :
        const Model* _model; ///< Read only access to the model

    public :
        /*! 
         *  \brief View constructor
         *  \param[in] model : The model is needed to construct the view
         */
    	View(const Model &model);
    	/*! 
         *  \brief View destructor
         *  Note that this destructor is virtual and has no code.
         */
    	virtual ~View();

        /*! 
         *  \brief Draws everything. Entry point of the view.
         */
        virtual void draw() = 0;
};


#endif
