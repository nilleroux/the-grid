/*! \file ScoresList.cpp
 * \brief See the corresponding header file.
 * \author { Sylvain Chiron, Nils Le Roux }
 */

#include "ScoresList.hpp"


const int ScoresList::_cellValueFactor = 10;


ScoresList::ScoresList(int lives)
        : _lives(lives), _totalScore(0), _moves(0), _levelScore(0), _nbBonus(0), _timeBonus(0), _scoreBonus(0), _livesBonus(0),
          _timeLeftBonus(0), _campaignBonus(0)
{}


void ScoresList::resetLevelScores()
{
    _moves = _levelScore = _nbBonus = 0;
    _timeBonus = std::chrono::milliseconds(0);
    _scoreBonus = _livesBonus = _timeLeftBonus = 0;
}


int ScoresList::getLives() const
{
    return _lives;
}

int ScoresList::getTotalScore() const
{
    return _totalScore;
}

int ScoresList::getMoves() const
{
    return _moves;
}

int ScoresList::getLevelScore() const
{
    return _levelScore;
}

int ScoresList::getNbBonus() const
{
    return _nbBonus;
}

int ScoresList::getScoreBonus() const
{
    return _scoreBonus;
}

const std::chrono::milliseconds &ScoresList::getTimeBonus() const
{
    return _timeBonus;
}

int ScoresList::getLivesBonus() const
{
    return _livesBonus;
}

int ScoresList::getTimeLeftBonus() const
{
    return _timeLeftBonus;
}

int ScoresList::getCampaignBonus() const
{
    return _campaignBonus;
}


void ScoresList::loseLife()
{
    _lives--;
}

void ScoresList::incrementMoves()
{
    _moves++;
}

void ScoresList::incrementCellScore(int value)
{
    _levelScore += value * _cellValueFactor;
    _totalScore += value * _cellValueFactor;
}

void ScoresList::pickScoreBonus(int value)
{
    _nbBonus++;
    _scoreBonus += value;
    _levelScore += value;
    _totalScore += value;
}

void ScoresList::pickTimeBonus(const std::chrono::milliseconds &value)
{
    _nbBonus++;
    _timeBonus += value;
}

void ScoresList::pickLifeBonus()
{
    _nbBonus++;
    _livesBonus++;
    _lives++;
}

void ScoresList::addTimeLeftBonus(const std::chrono::milliseconds &time_left)
{
    int score_to_add = time_left.count() / 50;
    _timeLeftBonus += score_to_add;
    _levelScore += score_to_add;
    _totalScore += score_to_add;
}

void ScoresList::addCampaignBonus(int value)
{
    _campaignBonus += value;
    _totalScore += value;
}


void ScoresList::recoverLife()
{
    _lives++;
}

void ScoresList::undoMove()
{
    _moves--;
}

void ScoresList::undoCellScore(int value)
{
    incrementCellScore(-value);
}

void ScoresList::undoScoreBonus(int value)
{
    _nbBonus--;
    _scoreBonus -= value;
    _levelScore -= value;
    _totalScore -= value;
}

void ScoresList::undoTimeBonus(const std::chrono::milliseconds &value)
{
    _nbBonus--;
    _timeBonus -= value;
}

void ScoresList::undoLifeBonus()
{
    _nbBonus--;
    _livesBonus--;
    _lives--;
}
